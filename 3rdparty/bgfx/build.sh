#!/bin/bash

if [ "$1" != "FROM_DOCKER" ]; then
	docker build "$PWD"
	docker run -it --rm -v "$PWD:/usr/local/src" $(docker build -q "$PWD") /bin/bash /usr/local/src/build.sh "FROM_DOCKER"
	exit $?
fi

cd /usr/local/src/

if [ -e "bx/.git" ]; then
	git -C "bx" pull
else
	git clone git://github.com/bkaradzic/bx.git "bx"
fi

if [ -e "bimg/.git" ]; then
	git -C "bimg" pull
else
	git clone git://github.com/bkaradzic/bimg.git "bimg"
fi

if [ -e "bgfx/.git" ]; then
	git -C "bgfx" pull
else
	git clone git://github.com/bkaradzic/bgfx.git "bgfx"
fi

cd bgfx
make

make linux-release64
