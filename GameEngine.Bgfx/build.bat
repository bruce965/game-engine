@echo off
setlocal EnableDelayedExpansion
set INITIAL_DIRECTORY=%cd%
cd /d "%~dp0"

set SHADERC=../3rdparty/SharpBgfx/tools/shaderc.exe
set TEXTUREC=../3rdparty/SharpBgfx/tools/texturec.exe
set INCLUDE_PATH=../3rdparty/SharpBgfx/include

echo Cleaning...
rmdir /s /q "Shaders/bin" >nul
rmdir /s /q "Textures/bin" >nul
echo.

echo Building Shaders...

mkdir "Shaders/bin/d3d9"
mkdir "Shaders/bin/d3d11"
for /f "usebackq delims=|" %%f in (`dir /a-d /b /s "Shaders/src"`) do (
	rem only build files with ".sc" extension
	if "%%~xf" equ ".sc" (
		set filename=%%~nf

		rem file name starting with "vs_" for fragment shaders
		if "!filename:~0,3!" == "vs_" (
			echo %%~nxf
			"%SHADERC%" -i "%INCLUDE_PATH%" -f "%%f" -o "Shaders/bin/d3d9/%%~nf.bin" --type vertex --platform windows -p vs_3_0 -O 3
			"%SHADERC%" -i "%INCLUDE_PATH%" -f "%%f" -o "Shaders/bin/d3d11/%%~nf.bin" --type vertex --platform windows -p vs_4_0 -O 3
		)

		rem file name starting with "fs_" for fragment shaders
		if "!filename:~0,3!" == "fs_" (
			echo %%~nxf
			"%SHADERC%" -i "%INCLUDE_PATH%" -f "%%f" -o "Shaders/bin/d3d9/%%~nf.bin" --type fragment --platform windows -p ps_3_0 -O 3
			"%SHADERC%" -i "%INCLUDE_PATH%" -f "%%f" -o "Shaders/bin/d3d11/%%~nf.bin" --type fragment --platform windows -p ps_4_0 -O 3
		)

		rem TODO: Linux, other DirectX versions, others.
	)
)
echo.

echo Building Textures...
mkdir "Textures/bin"
for /f "usebackq delims=|" %%f in (`dir /a-d /b /s "Textures/src"`) do (
	echo %%~nxf
	"%TEXTUREC%" -f "%%f" -o "Textures/bin/%%~nf.dds" --as DDS
)
echo.

echo Done
cd /d "%INITIAL_DIRECTORY%"
echo.
