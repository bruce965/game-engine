using System;
using System.Numerics;
using System.Runtime.InteropServices;
using Bgfx;
using GameEngine.Bgfx.Shaders;
using GameEngine.Bgfx.Textures;
using GameEngine.Common;
using GameEngine.ECS;
using GameEngine.Graphics.Graphics2D;

namespace GameEngine.Bgfx.Internal
{
	public unsafe sealed class BgfxSprite2DRenderingSystem : ISystem, IDisposable
	{
		[StructLayout(LayoutKind.Sequential)]
		struct Vertex
		{
			public static bgfx.VertexLayout Layout { get; }

			static Vertex() {
				bgfx.VertexLayout layout;
				bgfx.vertex_layout_begin(&layout, bgfx.get_renderer_type());
				bgfx.vertex_layout_add(&layout, bgfx.Attrib.Position, 2, bgfx.AttribType.Float, false, false);
				bgfx.vertex_layout_add(&layout, bgfx.Attrib.TexCoord0, 2, bgfx.AttribType.Float, false, false);
				bgfx.vertex_layout_end(&layout);
				Layout = layout;
			}

			public float X;
			public float Y;
			public float U;
			public float V;
		}

		readonly bgfx.ProgramHandle program = bgfx.create_program(ShaderLoader.Load("vs_sprites"), ShaderLoader.Load("fs_sprites"), true);
		readonly bgfx.TextureHandle missingTexture = TextureLoader.Load("missing");
		readonly bgfx.UniformHandle texColor = bgfx.create_uniform("s_texColor", bgfx.UniformType.Sampler, 1);

		IReadOnlyComponent<Transform2D> Transform2D;
		IReadOnlyComponent<Sprite2D> Sprite2D;
		IViewportData Viewport;

		void ISystem.Initialize(ISystemInitializer system)
		{
			Transform2D = system.ReadsFrom<Transform2D>();
			Sprite2D = system.ReadsFrom<Sprite2D>();
			Viewport = system.ReadsSharedData<IViewportData>();
		}

		void ISystem.Execute()
		{
			var sprites = Sprite2D.ToSpan();
			if (sprites.Length > 0)
			{
				var vertexLayout = Vertex.Layout;

				var group = sprites.Slice(0, 1);  // TODO: enable GroupByRef.
				// GroupByRef.Do(sprites, component => component.Data.Texture.Bgfx?.GetHashCode() ?? -1, groups =>
				// {
				// 	foreach (var group in groups)
				// 	{
						bgfx.TransientVertexBuffer vertexBuffer;
						bgfx.TransientIndexBuffer indexBuffer;
						if (!bgfx.alloc_transient_buffers(&vertexBuffer, &vertexLayout, (uint)(group.Length * 4), &indexBuffer, (uint)(group.Length * 3 * 2)))
							return;  // TODO: improve error recovery

						// populate buffers for sprites
						var vertices = new Span<Vertex>(vertexBuffer.data, (int)vertexBuffer.size);
						var indices = new Span<ushort>(indexBuffer.data, (int)indexBuffer.size);
						var v = 0;
						var i = 0;
						foreach (var sprite in group)
						{
							// TODO: error checking and optimization
							var matrix = Transform2D.TryGetFromEntity(sprite.EntityId, out var _).Data.GetAbsoluteTransform(Transform2D);
							var translation = matrix.Translation;

							// 0 _____ 1
							//  |    /|
							//  |  /  |
							//  |/____|
							// 2       3

							// Vertex 0 (top-left)
							vertices[v + 0] = new Vertex
							{
								X = translation.X,
								Y = translation.Y,
								U = sprite.Data.UVTopLeft.X,
								V = sprite.Data.UVTopLeft.Y,
							};

							// Vertex 1 (top-right)
							vertices[v + 1] = new Vertex
							{
								X = translation.X + sprite.Data.Size.X,
								Y = translation.Y,
								U = sprite.Data.UVBottomRight.X,
								V = sprite.Data.UVTopLeft.Y,
							};

							// Vertex 2 (bottom-left)
							vertices[v + 2] = new Vertex
							{
								X = translation.X,
								Y = translation.Y + sprite.Data.Size.Y,
								U = sprite.Data.UVTopLeft.X,
								V = sprite.Data.UVBottomRight.Y,
							};

							// Vertex 3 (bottom-right)
							vertices[v + 3] = new Vertex
							{
								X = translation.X + sprite.Data.Size.X,
								Y = translation.Y + sprite.Data.Size.Y,
								U = sprite.Data.UVBottomRight.X,
								V = sprite.Data.UVBottomRight.Y,
							};

							// Triangle 0
							indices[i + 0] = (ushort)(v + 0);
							indices[i + 1] = (ushort)(v + 2);
							indices[i + 2] = (ushort)(v + 1);

							// Triangle 1
							indices[i + 3] = (ushort)(v + 1);
							indices[i + 4] = (ushort)(v + 2);
							indices[i + 5] = (ushort)(v + 3);

							v += 4;
							i += 3 * 2;
						}

						// var transform = Matrix4x4.CreateOrthographicOffCenter(0, 100f, 100f, 0f, -1f, 1f);
						// Bgfx.SetTransform(&transform.M11);

						var identity = Matrix4x4.Identity;
						var test = Matrix4x4.CreateOrthographicOffCenter(0f, Viewport.Width, Viewport.Height, 0f, 0.5f, -0.5f);
						bgfx.set_view_transform(0, &identity.M11, &test.M11);

						bgfx.set_texture(0, texColor, (group[0].Data.Texture as Texture2D)?.Bgfx ?? missingTexture, (uint)bgfx.SamplerFlags.None);

						// var transform = Matrix4x4.CreateOrthographicOffCenter(0f, Viewport.Width, Viewport.Height, 0f, 0f, 1000f);
						// Bgfx.SetViewTransform(0, null, &transform.M11);
						//Bgfx.SetViewRect(0, 0, 0, Viewport.Width, Viewport.Height);

						bgfx.set_vertex_buffer(0, vertexBuffer.handle, 0, vertexBuffer.size);
						bgfx.set_index_buffer(indexBuffer.handle, 0, indexBuffer.size);
						bgfx.set_state((ulong)bgfx.StateFlags.Default, 0);
						bgfx.submit(0, program, 0, (byte)bgfx.DiscardFlags.None);
				// 	}
				// });
			}
		}

		#region IDisposable Support

		bool disposedValue = false;

		private /*protected virtual*/ void Dispose(bool disposing)
		{
			if (!disposedValue)
			{
				if (disposing)
				{
					// dispose managed state (managed objects)
					// (none)
				}

				// free unmanaged resources (unmanaged objects)
				bgfx.destroy_program(program);
				bgfx.destroy_texture(missingTexture);
				bgfx.destroy_uniform(texColor);

				disposedValue = true;
			}
		}

		~BgfxSprite2DRenderingSystem() => Dispose(false);

		public void Dispose()
		{
			Dispose(true);
			GC.SuppressFinalize(this);
		}

		#endregion
	}
}
