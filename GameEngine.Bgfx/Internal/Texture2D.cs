using Bgfx;
using GameEngine.Graphics;

namespace GameEngine.Bgfx.Internal
{
	sealed class Texture2D : ITexture2D
	{
		public readonly bgfx.TextureHandle Bgfx;

		public Texture2D(bgfx.TextureHandle bgfx)
		{
			this.Bgfx = bgfx;
		}
	}
}
