using System.Runtime.InteropServices;

namespace Bgfx
{
	partial class bgfx
	{
		const string DllName
		#if DEBUG
			= "libbgfx-shared-libRelease.so";
		#else
			= "libbgfx-shared-libRelease.so";
		#endif

		[DllImport(DllName, EntryPoint="bgfx_dbg_text_printf", CallingConvention = CallingConvention.Cdecl)]
		public static extern unsafe void dbg_text_printf(ushort _x, ushort _y, byte _attr, byte* _format, byte* args);
	}
}
