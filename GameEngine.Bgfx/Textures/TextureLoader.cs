using System;
using System.Buffers;
using Bgfx;

namespace GameEngine.Bgfx.Textures
{
	static class TextureLoader
	{
		public static unsafe bgfx.TextureHandle Load(string name, bgfx.TextureFlags flags = bgfx.TextureFlags.None, int skipMips = 0)
		{
			var path = $"{nameof(GameEngine)}.{nameof(GameEngine.Bgfx)}.{nameof(GameEngine.Bgfx.Textures)}.bin.{name}.dds";

			using var stream = typeof(TextureLoader).Assembly.GetManifestResourceStream(path);

			var mem = bgfx.alloc((uint)stream.Length);
			var buffer = new Span<byte>(mem->data, (int)mem->size);

			int totalRead = 0;
			int read;
			while ((read = stream.Read(buffer[totalRead..])) > 0)
				totalRead += read;

			return bgfx.create_texture(mem, (ulong)flags, (byte)skipMips, null);
		}
	}
}
