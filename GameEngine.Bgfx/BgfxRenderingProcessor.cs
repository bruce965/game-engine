using System.Linq;
using GameEngine.Bgfx.Internal;
using GameEngine.ECS;
using GameEngine.Graphics.Graphics2D;

namespace GameEngine.Bgfx
{
	/// <summary>Installs the <see cref="ISystem"> instances required to optimally render the universe through Bgfx.</summary>
	public sealed class BgfxRenderingProcessor : IProcessor
	{
		void IProcessor.Initialize(IUniverse universe)
		{
			// only register sprite rendering system if sprites are registered
			if (universe.Components.Any(c => c.Type == typeof(Sprite2D)))
			{
				// run the rendering sprite after everything else (last "game logic" system + 1000)
				universe.RegisterSystem(new BgfxSprite2DRenderingSystem(), ExecutionOrder.Last + 1000);
			}
		}
	}
}
