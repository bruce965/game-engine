using System;
using Bgfx;

namespace GameEngine.Bgfx.Shaders
{
	static class ShaderLoader
	{
		static string GetCurrentRendererName()
		{
			switch (bgfx.get_renderer_type())
			{
				case bgfx.RendererType.Direct3D9:
					return "d3d9";

				case bgfx.RendererType.Direct3D11:
				case bgfx.RendererType.Direct3D12:
					return "d3d11";

				// case bgfx.RendererType.Gnm:
				// 	return "gnm";

				// case bgfx.RendererType.Metal:
				// 	return "metal";

				// case bgfx.RendererType.OpenGLES:
				// 	return "opengles";

				case bgfx.RendererType.OpenGL:
					return "opengl";

				// case bgfx.RendererType.Vulkan:
				// 	return "vulkan";

				case bgfx.RendererType.Noop:
				default:
					return null;
			}
		}

		public static unsafe bgfx.ShaderHandle Load(string name)
		{
			var path = $"{nameof(GameEngine)}.{nameof(GameEngine.Bgfx)}.{nameof(GameEngine.Bgfx.Shaders)}.bin.{GetCurrentRendererName()}.{name}.bin";

			using var stream = typeof(ShaderLoader).Assembly.GetManifestResourceStream(path);

			var mem = bgfx.alloc((uint)stream.Length);
			var buffer = new Span<byte>(mem->data, (int)mem->size);

			int totalRead = 0;
			int read;
			while ((read = stream.Read(buffer[totalRead..])) > 0)
				totalRead += read;

			return bgfx.create_shader(mem);
		}
	}
}
