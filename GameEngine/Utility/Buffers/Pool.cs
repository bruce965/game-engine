using System;
using System.Collections.Concurrent;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Threading;

namespace GameEngine.Utility.Buffers
{
	public abstract class Pool<T> where T : class
	{
		static Pool<T> shared;
		public static Pool<T> Shared
		{
			get
			{
				// https://github.com/dotnet/corefx/blob/5168f541d4def7c54e490a685ff5dc0e3f90e91a/src/System.Buffers/src/System/Buffers/ArrayPool.cs#L42

				var shared = Volatile.Read(ref Pool<T>.shared);
				if (shared == null)
				{
					var factory = GetFactory();
					if (factory == null)
						throw new InvalidOperationException("Types without a default (parameterless) constructor don't have a shared pool.");

					Interlocked.CompareExchange(ref Pool<T>.shared, Pool.Create<T>(factory), null);
					shared = Pool<T>.shared;
				}

				return shared;
			}
		}

		public abstract T Rent();

		public abstract void Return(T value);

		static Func<T> GetFactory()
		{
			var ctor = typeof(T).GetConstructor(Type.EmptyTypes);
			if (ctor == null)
				return null;

			var construct = Expression.Lambda<Func<T>>(Expression.New(ctor), Enumerable.Empty<ParameterExpression>());
			return construct.Compile();
		}
	}

	public static class Pool
	{
		class DefaultPool<T> : Pool<T> where T : class
		{
			readonly BlockingCollection<T> collection;
			readonly Func<T> factory;

			public DefaultPool(int maxElementsCount, Func<T> factory)
			{
				this.collection = new BlockingCollection<T>(new ConcurrentBag<T>(), maxElementsCount);
				this.factory = factory;
			}

			public override T Rent()
			{
				if (!collection.TryTake(out T value))
					value = factory();

				return value;
			}

			public override void Return(T value)
			{
				collection.TryAdd(value);
			}
		}

		public static Pool<T> Create<T>() where T : class, new() => Create<T>(1024 * 1024);

		public static Pool<T> Create<T>(int maxElementsCount) where T : class, new() => Create<T>(maxElementsCount, () => new T());

		public static Pool<T> Create<T>(int maxElementsCount, Func<T> factory) where T : class => new DefaultPool<T>(maxElementsCount, factory);

		public static Pool<T> Create<T>(Func<T> factory) where T : class => Create<T>(1024 * 1024, factory);
	}
}
