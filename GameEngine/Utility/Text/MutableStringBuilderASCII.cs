using System;
using System.Diagnostics;
using System.Text;

namespace GameEngine.Utility.Text
{
	[DebuggerDisplay("{MutableString.ToString()}")]
	public unsafe ref struct MutableStringBuilderASCII
	{
		public readonly MutableString MutableString;

		public MutableStringBuilderASCII(MutableString asciiString)
		{
			if (asciiString.Encoding != Encoding.ASCII)
				throw new ArgumentException($"{nameof(MutableStringBuilderASCII)} only suppports ASCII strings.", nameof(asciiString));

			this.MutableString = asciiString;
		}

		#region public MutableStringBuilderASCII Append(Something s);

		public MutableStringBuilderASCII Append(bool b) => Append(b ? "true" : "false");

		public MutableStringBuilderASCII Append(byte b) => Append((long)b);

		public MutableStringBuilderASCII Append(DateTime d)
		{
			// TODO
			//Append(d.ToString(CultureInfo.InvariantCulture));
			return this;
		}

		public MutableStringBuilderASCII Append(DateTimeOffset d)
		{
			// TODO
			//Append(d.ToString(CultureInfo.InvariantCulture));
			return this;
		}

		public MutableStringBuilderASCII Append(TimeSpan t)
		{
			// TODO
			//Append(t.ToString(CultureInfo.InvariantCulture));
			return this;
		}

		public MutableStringBuilderASCII Append(decimal d)
		{
			// TODO
			//Append(d.ToString(CultureInfo.InvariantCulture));
			return this;
		}

		public MutableStringBuilderASCII Append(double d)
		{
			if (d < 0)
			{
				Append('-');
				d = -d;
			}

			if (Double.IsNaN(d))
			{
				Append("NaN");
				return this;
			}
			else if (Double.IsInfinity(d))
			{
				Append("Infinity");
				return this;
			}
			else if (d == 0)
			{
				Append('0');
				return this;
			}

			// TODO: handle non-integer numbers.

			Span<byte> buff = stackalloc byte[200];

			var digits = 0;
			while (d > 0)
			{
				var dv = d;
				d = Math.Floor(d / 10);

				var v = (int)dv - (int)d * 10;
				buff[digits] = (byte)('0' + v);
				digits++;
			}

			var slice = buff.Slice(0, digits);
			slice.Reverse();

			var buffer = MutableString.EnsureAdditionalBufferBytes(slice.Length);
			slice.CopyTo(buffer.AsSpan(MutableString.Count));
			MutableString.Count += slice.Length;

			//fixed (byte* b = &MemoryMarshal.GetReference(buff))
			//	Utf8String.Length += Encoding.GetCharCount(b, slice.Length);

			return this;
		}

		public MutableStringBuilderASCII Append(short s) => Append((long)s);

		public MutableStringBuilderASCII Append(int i) => Append((long)i);

		public MutableStringBuilderASCII Append(long l)
		{
			if (l < 0)
			{
				Append('-');
				l = -l;
			}

			if (l == 0)
			{
				Append('0');
				return this;
			}

			Span<byte> buff = stackalloc byte[20];

			var digits = 0;
			while (l > 0)
			{
				var lv = l;
				l /= 10;

				var v = lv - l * 10;
				buff[digits] = (byte)('0' + v);
				digits++;
			}

			var slice = buff.Slice(0, digits);
			slice.Reverse();

			var buffer = MutableString.EnsureAdditionalBufferBytes(slice.Length);
			slice.CopyTo(buffer.AsSpan(MutableString.Count));
			MutableString.Count += slice.Length;

			//fixed (byte* b = &MemoryMarshal.GetReference(buff))
			//	Utf8String.Length += Encoding.GetCharCount(b, slice.Length);

			return this;
		}

		public MutableStringBuilderASCII Append(sbyte s) => Append((long)s);

		public MutableStringBuilderASCII Append(float f) => Append((double)f);

		public MutableStringBuilderASCII Append(ushort u) => Append((long)u);

		public MutableStringBuilderASCII Append(uint u) => Append((long)u);

		public MutableStringBuilderASCII Append(ulong u)
		{
			// TODO
			//Append(u.ToString(CultureInfo.InvariantCulture));
			return this;
		}

		public MutableStringBuilderASCII Append(string s)
		{
			if (s != null)
			{
				var buffer = MutableString.EnsureAdditionalBufferBytes(s.Length + 1);  // +1 for null terminator
				var newBytesCount = Encoding.ASCII.GetBytes(s, 0, s.Length, buffer, MutableString.Count);

				//MutableString.Length += s.Length;
				MutableString.Count += newBytesCount;
			}

			return this;
		}

		public MutableStringBuilderASCII Append(char c)
		{
			var buffer = MutableString.EnsureAdditionalBufferBytes(1 + 1);  // +1 for null terminator

			fixed (byte* b = &buffer[MutableString.Count])
				MutableString.Count += Encoding.ASCII.GetBytes(&c, 1, b, 1);

			//MutableString.Length += 1;

			return this;
		}

		public MutableStringBuilderASCII Append(MutableString m)
		{
			if (m != null)
			{
				if (m.Encoding == MutableString.Encoding)
				{
					// if encoding is the same, we can simply copy
					var buffer = MutableString.EnsureAdditionalBufferBytes(m.Count + 1);  // +1 for null terminator
					m.GetBuffer().AsSpan(0, m.Count).CopyTo(buffer.AsSpan(MutableString.Count));
				}
				else
				{
					// if encoding is different we convert to string, then encode the string
					Append(m.ToString());
				}
			}

			return this;
		}

		#endregion

		#region public MutableStringBuilderASCII Append<T1, T2, ..., TN>(T1 v1, T2 v2, ..., TN vN);

		public MutableStringBuilderASCII Append<T>(T value)
		{
			if (value is bool boolValue)
				Append(boolValue);
			else if (value is byte byteValue)
				Append(byteValue);
			else if (value is DateTime dateTimeValue)
				Append(dateTimeValue);
			else if (value is DateTimeOffset dateTimeOffsetValue)
				Append(dateTimeOffsetValue);
			else if (value is TimeSpan timeSpanValue)
				Append(timeSpanValue);
			else if (value is decimal decimalValue)
				Append(decimalValue);
			else if (value is double doubleValue)
				Append(doubleValue);
			else if (value is short shortValue)
				Append(shortValue);
			else if (value is int intValue)
				Append(intValue);
			else if (value is long longValue)
				Append(longValue);
			else if (value is sbyte sbyteValue)
				Append(sbyteValue);
			else if (value is ushort ushortValue)
				Append(ushortValue);
			else if (value is uint uintValue)
				Append(uintValue);
			else if (value is ulong ulongValue)
				Append(ulongValue);
			else if (value is char charValue)
				Append(charValue);
			else if (value is string stringValue)
				Append(stringValue);
			else if (value is MutableString mutableStringValue)
				Append(mutableStringValue);
			//else
			//	Append(value.ToString());  // TODO

			return this;
		}

		public MutableStringBuilderASCII Append<T1, T2>(T1 v1, T2 v2)
		{
			Append(v1);
			Append(v2);
			return this;
		}

		public MutableStringBuilderASCII Append<T1, T2, T3>(T1 v1, T2 v2, T3 v3)
		{
			Append(v1);
			Append(v2);
			Append(v3);
			return this;
		}

		public MutableStringBuilderASCII Append<T1, T2, T3, T4>(T1 v1, T2 v2, T3 v3, T4 v4)
		{
			Append(v1);
			Append(v2);
			Append(v3);
			Append(v4);
			return this;
		}

		public MutableStringBuilderASCII Append<T1, T2, T3, T4, T5>(T1 v1, T2 v2, T3 v3, T4 v4, T5 v5)
		{
			Append(v1);
			Append(v2);
			Append(v3);
			Append(v4);
			Append(v5);
			return this;
		}

		public MutableStringBuilderASCII Append<T1, T2, T3, T4, T5, T6>(T1 v1, T2 v2, T3 v3, T4 v4, T5 v5, T6 v6)
		{
			Append(v1);
			Append(v2);
			Append(v3);
			Append(v4);
			Append(v5);
			Append(v6);
			return this;
		}

		public MutableStringBuilderASCII Append<T1, T2, T3, T4, T5, T6, T7>(T1 v1, T2 v2, T3 v3, T4 v4, T5 v5, T6 v6, T7 v7)
		{
			Append(v1);
			Append(v2);
			Append(v3);
			Append(v4);
			Append(v5);
			Append(v6);
			Append(v7);
			return this;
		}

		#endregion

		public MutableStringBuilderASCII Clear()
		{
			MutableString.Count = 0;
			// NOTE: we don't release the buffer

			return this;
		}

		public void Terminate()
		{
			var buffer = MutableString.EnsureAdditionalBufferBytes(1);
			buffer[MutableString.Count] = 0;  // NULL terminator
			// NOTE: character count and buffer position are not increased
		}
	}
}
