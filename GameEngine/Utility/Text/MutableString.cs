using System;
using System.Buffers;
using System.Diagnostics;
using System.Text;

namespace GameEngine.Utility.Text
{
	[DebuggerDisplay("{ToString()}")]
	public class MutableString : IDisposable
	{
		int count;
		/// <summary>Number of bytes in this UTF-8 string.</summary>
		public int Count
		{
			get => count;
			set
			{
				EnsureTotalBufferBytes(value);
				count = value;
			}
		}

		///// <summary>Number of characters in this UTF-8 string.</summary>
		//public int Length { get; set; }

		public Encoding Encoding { get; }

		readonly ArrayPool<byte> pool;
		readonly bool clearBuffer;
		byte[] buffer;

		public MutableString(Encoding encoding, ArrayPool<byte> pool, bool clearBuffer)
		{
			this.Encoding = encoding;
			this.pool = ArrayPool<byte>.Shared;
			this.clearBuffer = false;
		}

		public MutableString(Encoding encoding) : this(encoding, null, false) { }

		public byte[] EnsureAdditionalBufferBytes(int additionalBytesCount)
		{
			var freeBufferBytes = (buffer?.Length ?? 0) - Count;
			if (freeBufferBytes >= additionalBytesCount)
			{
				// current buffer is already big enough
				return buffer;
			}

			// save reference to old buffer
			var previousBuffer = buffer;

			// acquire a new buffer
			var newLength = buffer == null ? additionalBytesCount : Math.Max(Count + additionalBytesCount, buffer.Length * 2);
			buffer = pool == null ? new byte[newLength] : pool.Rent(newLength);

			if (previousBuffer != null)
			{
				// copy old buffer to new buffer
				Array.Copy(previousBuffer, 0, buffer, 0, Count);

				// release old buffer
				if (pool != null)
					pool.Return(previousBuffer, clearBuffer);
			}

			return buffer;
		}

		public byte[] EnsureTotalBufferBytes(int bytesCount) => EnsureAdditionalBufferBytes(bytesCount - buffer?.Length ?? 0);

		public byte[] GetBuffer() => EnsureAdditionalBufferBytes(0);

		public override string ToString() => buffer == null ? null : Encoding.GetString(buffer, 0, Count);

		void IDisposable.Dispose()
		{
			if (buffer != null && pool != null)
				pool.Return(buffer, clearBuffer);

			buffer = null;
			Count = 0;
		}
	}
}
