using System;
using System.Buffers;

namespace GameEngine.Utility.Collections
{
	/// <summary>
	/// A list that relies on pools to reduce pressure on the garbage collector.
	/// </summary>
	public class/*ref struct*/ PooledList<T> //: IList<T>, IReadOnlyList<T>
	{
		ArrayPool<T> pool;
		bool clearArray;
		T[] backingArray;
		int count;

		public PooledList(ArrayPool<T> pool, bool clearArray = false)
		{
			this.pool = pool;
			this.clearArray = clearArray;
			this.backingArray = pool.Rent(32);
			this.count = 0;
		}

		public Memory<T> AsMemory()=> backingArray.AsMemory().Slice(0, count);

		public Span<T> AsSpan() => backingArray.AsSpan().Slice(0, count);

		public Span<T>.Enumerator GetEnumerator() => AsSpan().GetEnumerator();

		/// <summary>Return to the pool.</summary>
		public void ReturnToPool()
		{
			pool.Return(backingArray, clearArray);

			this.pool = null;
			//this.clearArray = false;
			this.backingArray = null;
			this.count = 0;
		}

		#region IList<T>

		public T this[int index] {
			get
			{
				if (index > count)
					throw new ArgumentOutOfRangeException(nameof(index));

				return backingArray[index];
			}
			set
			{
				if (index < 0 || index > count)
					throw new ArgumentOutOfRangeException(nameof(index));

				backingArray[index] = value;
			}
		}

		public int Count => count;

		public bool IsReadOnly => false;

		public void Add(T item)
		{
			if (count >= backingArray.Length)
			{
				var newBackingArray = pool.Rent(count * 2);
				backingArray.CopyTo(newBackingArray, 0);

				pool.Return(backingArray, clearArray);
				backingArray = newBackingArray;
			}

			backingArray[count++] = item;
		}

		public void Clear() => count = 0;

		public bool Contains(T item) => IndexOf(item) >= 0;

		public void CopyTo(T[] array, int arrayIndex) => Array.Copy(backingArray, 0, array, arrayIndex, count);

		public int IndexOf(T item) => Array.IndexOf(backingArray, item, 0, count);

		public void Insert(int index, T item)
		{
			if (index < 0 || index > count)
				throw new ArgumentOutOfRangeException(nameof(index));

			if (count >= backingArray.Length)
			{
				var newBackingArray = pool.Rent(count * 2);
				backingArray.CopyTo(newBackingArray, 0);

				pool.Return(backingArray, clearArray);
				backingArray = newBackingArray;
			}

			Array.Copy(backingArray, index, backingArray, index + 1, count - index);
			backingArray[index] = item;
			count++;
		}

		public bool Remove(T item)
		{
			var index = IndexOf(item);
			if (index < 0)
				return false;

			RemoveAt(index);
			return true;
		}

		public void RemoveAt(int index)
		{
			if (index < 0 || index > count)
				throw new ArgumentOutOfRangeException(nameof(index));

			Array.Copy(backingArray, index + 1, backingArray, index, count - index - 1);
			count--;
		}

		//IEnumerator<T> IEnumerable<T>.GetEnumerator() => backingArray.Take(count).GetEnumerator();

		//IEnumerator IEnumerable.GetEnumerator() => ((IEnumerable<T>)this).GetEnumerator();

		#endregion
	}
}
