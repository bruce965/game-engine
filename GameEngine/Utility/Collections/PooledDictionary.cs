using System;
using System.Collections.Generic;

namespace GameEngine.Utility.Collections
{
	/// <summary>
	/// A dictionary that relies on pools to reduce pressure on the garbage collector.
	/// </summary>
	public ref struct PooledDictionary<TKey, TValue> //: IDictionary<TKey, TValue>, IReadOnlyDictionary<TKey, TValue>
	{
		// TODO

		#region IDictionary<TKey, TValue>

		public TValue this[TKey key] { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }

		public ICollection<TKey> Keys => throw new NotImplementedException();

		public ICollection<TValue> Values => throw new NotImplementedException();

		public int Count => throw new NotImplementedException();

		public bool IsReadOnly => throw new NotImplementedException();

		//IEnumerable<TKey> IReadOnlyDictionary<TKey, TValue>.Keys => throw new NotImplementedException();

		//IEnumerable<TValue> IReadOnlyDictionary<TKey, TValue>.Values => throw new NotImplementedException();

		public void Add(TKey key, TValue value)
		{
			throw new NotImplementedException();
		}

		public void Add(KeyValuePair<TKey, TValue> item)
		{
			throw new NotImplementedException();
		}

		public void Clear()
		{
			throw new NotImplementedException();
		}

		public bool Contains(KeyValuePair<TKey, TValue> item)
		{
			throw new NotImplementedException();
		}

		public bool ContainsKey(TKey key)
		{
			throw new NotImplementedException();
		}

		public void CopyTo(KeyValuePair<TKey, TValue>[] array, int arrayIndex)
		{
			throw new NotImplementedException();
		}

		public IEnumerator<KeyValuePair<TKey, TValue>> GetEnumerator()
		{
			throw new NotImplementedException();
		}

		public bool Remove(TKey key)
		{
			throw new NotImplementedException();
		}

		public bool Remove(KeyValuePair<TKey, TValue> item)
		{
			throw new NotImplementedException();
		}

		public bool TryGetValue(TKey key, out TValue value)
		{
			throw new NotImplementedException();
		}

		//IEnumerator IEnumerable.GetEnumerator() => throw new NotImplementedException();

		#endregion
	}
}
