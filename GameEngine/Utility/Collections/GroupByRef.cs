using System;
using System.Buffers;
using System.Collections.Generic;

namespace GameEngine.Utility.Collections
{
	/// <summary>
	/// A faster, more limited version of the <see cref="System.Linq.Enumerable.GroupBy"/>.
	/// </summary>
	public static class GroupByRef
	{
		public delegate TKey GetKey<T, TKey>(T data);

		public delegate void Callback<T, TKey>(Groups<T, TKey> groups);
		public delegate void ReadOnlyCallback<T, TKey>(ReadOnlyGroups<T, TKey> groups);

		/// <summary>
		/// A group of elements with the same key.
		/// </summary>
		public ref struct Group<T, TKey>
		{
			public TKey Key => getKey(source[indices[0]]);

			public int Count => indices.Length;

			public ref T this[int index] => ref source[indices[index]];

			Span<T> source;
			ReadOnlySpan<int> indices;
			GetKey<T, TKey> getKey;

			internal Group(Span<T> source, ReadOnlySpan<int> indices, GetKey<T, TKey> getKey)
			{
				this.source = source;
				this.indices = indices;
				this.getKey = getKey;
			}

			public Enumerator GetEnumerator() => new Enumerator(source, indices);

			public ref struct Enumerator
			{
				public ref T Current => ref source[indices[currentIndex]];

				Span<T> source;
				ReadOnlySpan<int> indices;
				int currentIndex;

				internal Enumerator(Span<T> source, ReadOnlySpan<int> indices)
				{
					this.source = source;
					this.indices = indices;
					this.currentIndex = 0;
				}

				public bool MoveNext() => ++currentIndex < indices.Length;
			}
		}

		/// <summary>
		/// A group of elements with the same key.
		/// </summary>
		public ref struct ReadOnlyGroup<T, TKey>
		{
			public TKey Key => getKey(source[indices[0]]);

			public int Count => indices.Length;

			public ref readonly T this[int index] => ref source[indices[index]];

			ReadOnlySpan<T> source;
			ReadOnlySpan<int> indices;
			GetKey<T, TKey> getKey;

			internal ReadOnlyGroup(ReadOnlySpan<T> source, ReadOnlySpan<int> indices, GetKey<T, TKey> getKey)
			{
				this.source = source;
				this.indices = indices;
				this.getKey = getKey;
			}

			public Enumerator GetEnumerator() => new Enumerator(source, indices);

			public ref struct Enumerator
			{
				public ref readonly T Current => ref source[indices[currentIndex]];

				ReadOnlySpan<T> source;
				ReadOnlySpan<int> indices;
				int currentIndex;

				internal Enumerator(ReadOnlySpan<T> source, ReadOnlySpan<int> indices)
				{
					this.source = source;
					this.indices = indices;
					this.currentIndex = -1;
				}

				public bool MoveNext() => ++currentIndex < indices.Length;
			}
		}

		/// <summary>
		/// A set of groups, each containing only elements with the same key.
		/// </summary>
		public ref struct Groups<T, TKey>
		{
			Span<T> source;
			ReadOnlySpan<int> indices;
			ReadOnlySpan<int> groupIndices;
			GetKey<T, TKey> getKey;

			internal Groups(Span<T> source, ReadOnlySpan<int> indices, ReadOnlySpan<int> groupIndices, GetKey<T, TKey> getKey)
			{
				this.source = source;
				this.indices = indices;
				this.groupIndices = groupIndices;
				this.getKey = getKey;
			}

			public Enumerator GetEnumerator() => new Enumerator(source, indices, groupIndices, getKey);

			public ref struct Enumerator
			{
				public Group<T, TKey> Current => new Group<T, TKey>(source, indices.Slice(groupIndices[currentGroupIndex], groupIndices[currentGroupIndex+1] - groupIndices[currentGroupIndex]), getKey);

				Span<T> source;
				ReadOnlySpan<int> indices;
				ReadOnlySpan<int> groupIndices;
				GetKey<T, TKey> getKey;
				int currentGroupIndex;

				internal Enumerator(Span<T> source, ReadOnlySpan<int> indices, ReadOnlySpan<int> groupIndices, GetKey<T, TKey> getKey)
				{
					this.source = source;
					this.indices = indices;
					this.groupIndices = groupIndices;
					this.getKey = getKey;
					this.currentGroupIndex = -1;
				}

				public bool MoveNext() => ++currentGroupIndex < indices.Length;
			}
		}

		/// <summary>
		/// A set of groups, each containing only elements with the same key.
		/// </summary>
		public ref struct ReadOnlyGroups<T, TKey>
		{
			ReadOnlySpan<T> source;
			ReadOnlySpan<int> indices;
			ReadOnlySpan<int> groupIndices;
			GetKey<T, TKey> getKey;

			internal ReadOnlyGroups(ReadOnlySpan<T> source, ReadOnlySpan<int> indices, ReadOnlySpan<int> groupIndices, GetKey<T, TKey> getKey)
			{
				this.source = source;
				this.indices = indices;
				this.groupIndices = groupIndices;
				this.getKey = getKey;
			}

			public Enumerator GetEnumerator() => new Enumerator(source, indices, groupIndices, getKey);

			public ref struct Enumerator
			{
				public ReadOnlyGroup<T, TKey> Current => new ReadOnlyGroup<T, TKey>(source, indices.Slice(groupIndices[currentGroupIndex], groupIndices[currentGroupIndex+1] - groupIndices[currentGroupIndex]), getKey);

				ReadOnlySpan<T> source;
				ReadOnlySpan<int> indices;
				ReadOnlySpan<int> groupIndices;
				GetKey<T, TKey> getKey;
				int currentGroupIndex;

				internal Enumerator(ReadOnlySpan<T> source, ReadOnlySpan<int> indices, ReadOnlySpan<int> groupIndices, GetKey<T, TKey> getKey)
				{
					this.source = source;
					this.indices = indices;
					this.groupIndices = groupIndices;
					this.getKey = getKey;
					this.currentGroupIndex = -1;
				}

				public bool MoveNext() => ++currentGroupIndex < indices.Length;
			}
		}

		public static void Do<T, TKey>(Span<T> elements, GetKey<T, TKey> getKey, Callback<T, TKey> callback)
		{
			// TODO: var groupIndices = new PooledDictionary<TKey, PooledList<int>>();
			var groups = new Dictionary<TKey, PooledList<int>>();

			// compute groups
			for (var i = 0; i < elements.Length; i++)
			{
				var key = getKey(elements[i]);

				if (!groups.TryGetValue(key, out var indicesInGroup))
					groups[key] = indicesInGroup = new PooledList<int>(ArrayPool<int>.Shared);

				indicesInGroup.Add(i);
			}

			Span<int> indices = stackalloc int[elements.Length];
			Span<int> groupIndices = stackalloc int[groups.Count];

			var currentIndex = 0;
			var currentGroup = 0;
			foreach (var group in groups.Values)
			{
				group.AsSpan().CopyTo(indices.Slice(currentIndex));
				groupIndices[currentGroup++] = currentIndex;
				currentIndex += group.Count;

				group.ReturnToPool();
			}

			// TODO: groups.ReturnToPool();

			// we cannot return the result without copying it to the heap, we use a callback to preserve the stackalloc-ed data and avoid unnecessary copies
			var groupsStruct = new Groups<T, TKey>(elements, indices, groupIndices, getKey);
			callback(groupsStruct);
		}

		public static void Do<T, TKey>(ReadOnlySpan<T> elements, GetKey<T, TKey> getKey, ReadOnlyCallback<T, TKey> callback)
		{
			// TODO: var groupIndices = new PooledDictionary<TKey, PooledList<int>>();
			var groups = new Dictionary<TKey, PooledList<int>>();

			// compute groups
			for (var i = 0; i < elements.Length; i++)
			{
				var key = getKey(elements[i]);

				if (!groups.TryGetValue(key, out var indicesInGroup))
					groups[key] = indicesInGroup = new PooledList<int>(ArrayPool<int>.Shared);

				indicesInGroup.Add(i);
			}

			Span<int> indices = stackalloc int[elements.Length];
			Span<int> groupIndices = stackalloc int[groups.Count + 1];

			var currentIndex = 0;
			var currentGroup = 0;
			foreach (var group in groups.Values)
			{
				group.AsSpan().CopyTo(indices.Slice(currentIndex));
				groupIndices[currentGroup++] = currentIndex;
				currentIndex += group.Count;

				group.ReturnToPool();
			}

			groupIndices[currentGroup] = currentIndex;

			// TODO: groups.ReturnToPool();

			// we cannot return the result without copying it to the heap, we use a callback to preserve the stackalloc-ed data and avoid unnecessary copies
			var groupsStruct = new ReadOnlyGroups<T, TKey>(elements, indices, groupIndices, getKey);
			callback(groupsStruct);
		}
	}
}
