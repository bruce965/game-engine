using System;
using GameEngine.Diagnostics.Internal;

namespace GameEngine.Diagnostics.Logging
{
	public sealed class ConsoleLogger : ILogger
	{
		static readonly object lockme = new Object();

		void ILogger.Log(LogLevel level, object message)
		{
			// Format: "timestamp level source - message".

			// TODO: make non-blocking.

			var now = DateTime.UtcNow;
			var source = StackWalker.GetExternalCallerLocation();

			lock (lockme)
			{
				string bgColor = null;
				string fgColor = null;
				switch (level)
				{
					case LogLevel.Fatal:
						bgColor = "101";  // bright red
						fgColor = "30";  // black
						break;

					case LogLevel.Error:
						//bgColor = "40";  // black
						fgColor = "91";  // bright red
						break;

					case LogLevel.Warning:
						//bgColor = "40";  // black
						fgColor = "93";  // bright yellow
						break;

					case LogLevel.Info:
						//bgColor = "40";  // black
						//fgColor = "97";  // bright white
						break;

					case LogLevel.Debug:
						//bgColor = "40";  // black
						fgColor = "37";  // white (not bright)
						break;

					case LogLevel.Trace:
						//bgColor = "40";  // black
						fgColor = "36";  // cyan (not bright)
						break;
				}

				if (bgColor != null || fgColor != null)
				{
					Console.Write("\x1B[");

					if (bgColor != null)
					{
						Console.Write(bgColor);

						if (fgColor != null)
							Console.Write(';');
					}

					if (fgColor != null)
						Console.Write(fgColor);

					Console.Write('m');
				}

				Console.Write(now.ToString("yyyy-MM-dd HH':'mm':'ss.ffffff"));
				Console.Write(' ');

				Console.Write(Enum.GetName(typeof(LogLevel), level).ToUpperInvariant());
				Console.Write(' ');

				Console.Write(source);

				Console.Write(" - ");
				Console.Write(message);
				Console.WriteLine("\x1B[0m");  // restore default color
			}
		}
	}
}
