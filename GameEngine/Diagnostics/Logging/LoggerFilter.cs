using System;

namespace GameEngine.Diagnostics.Logging
{
	public sealed class LoggerFilter : ILogger
	{
		readonly ILogger baseLogger;
		readonly LogLevel atLeast;
		readonly LogLevel atMost;

		public LoggerFilter(ILogger baseLogger, LogLevel atLeast, LogLevel atMost)
		{
			this.baseLogger = baseLogger;
			this.atLeast = atLeast;
			this.atMost = atMost;
		}

		public LoggerFilter(ILogger baseLogger, LogLevel atLeast) : this(baseLogger, atLeast, LogLevel.All) { }

		void ILogger.Log(LogLevel level, object message)
		{
			if (level <= atLeast && level >= atMost)
			{
				baseLogger.Log(level, message);
			}
		}
	}
}
