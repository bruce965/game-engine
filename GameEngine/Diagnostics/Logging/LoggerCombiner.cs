using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.ExceptionServices;

namespace GameEngine.Diagnostics.Logging
{
	public sealed class LoggerCombiner : ILogger
	{
		readonly ILogger[] loggers;

		public LoggerCombiner(IEnumerable<ILogger> loggers)
		{
			this.loggers = loggers.ToArray();
		}

		void ILogger.Log(LogLevel level, object message)
		{
			List<Exception> exceptions = null;

			foreach (var logger in loggers)
			{
				try
				{
					logger.Log(level, message);
				}
				catch (Exception e)
				{
					if (exceptions == null)
						exceptions = new List<Exception>();

					exceptions.Add(e);
				}
			}

			// check if any of the loggers threw
			if (exceptions != null)
			{
				// if one logger threw, rethrow without changing the stack
				if (exceptions.Count == 1)
					ExceptionDispatchInfo.Capture(exceptions[0]).Throw();

				// if multiple loggers threw, throw an aggregate exception
				throw new AggregateException(exceptions);
			}
		}
	}
}
