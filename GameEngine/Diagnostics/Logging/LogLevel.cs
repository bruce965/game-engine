using System;

namespace GameEngine.Diagnostics.Logging
{
	/// <summary>
	/// Importance level of a log message. Lower = more important.
	/// </summary>
	public enum LogLevel : int
	{
		/// <summary>All log messages.</summary>
		All = Int32.MinValue,

		/// <summary>Fatal error forcing immediate service shutdown.</summary>
		Fatal = 1000,

		/// <summary>Error that caused failure of a single operation but does not compromise the system.</summary>
		Error = 2000,

		/// <summary>Unexpected situation that requires attention and might cause problems.</summary>
		Warning = 3000,

		/// <summary>Useful informations about status of the service and configuration options.</summary>
		Info = 4000,

		/// <summary>Diagnostic informations, only in debug builds.</summary>
		Debug = 5000,

		/// <summary>Tracing informations, only in trace builds.</summary>
		Trace = 6000,

		/// <summary>No log message.</summary>
		None = Int32.MaxValue
	}
}
