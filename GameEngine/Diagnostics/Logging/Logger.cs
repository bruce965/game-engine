﻿using System;
using System.Collections.Generic;

namespace GameEngine.Diagnostics.Logging
{
	public interface ILogger
	{
		void Log(LogLevel level, object message);
	}

	public static class Logger
	{
		public static ILogger Combine(IEnumerable<ILogger> loggers) => new LoggerCombiner(loggers);

		public static ILogger Combine(params ILogger[] loggers) => Combine((IEnumerable<ILogger>)loggers);

		public static ILogger Filter(this ILogger logger, LogLevel atLeast) => new LoggerFilter(logger, atLeast);

		public static ILogger Filter(this ILogger logger, LogLevel atLeast, LogLevel atMost) => new LoggerFilter(logger, atLeast, atMost);
	}
}
