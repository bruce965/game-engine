using System;

namespace GameEngine.Diagnostics.Logging
{
	public sealed class NullLogger : ILogger
	{
		public static readonly NullLogger Instance = new NullLogger();

		NullLogger() { }

		void ILogger.Log(LogLevel level, object message) { }
	}
}
