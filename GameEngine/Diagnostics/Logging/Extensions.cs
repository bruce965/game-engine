using System.Diagnostics;

namespace GameEngine.Diagnostics.Logging
{
	public static class Extensions
	{
		/// <summary>Report a fatal error forcing immediate service shutdown.</summary>
		public static void Fatal(this ILogger logger, object message) => logger.Log(LogLevel.Fatal, message);

		/// <summary>Report an error that caused failure of a single operation but does not compromise the system.</summary>
		public static void Error(this ILogger logger, object message) => logger.Log(LogLevel.Error, message);

		/// <summary>Report an unexpected situation that requires attention and might cause problems.</summary>
		public static void Warning(this ILogger logger, object message) => logger.Log(LogLevel.Warning, message);

		/// <summary>Report useful informations about status of the service and configuration options.</summary>
		public static void Info(this ILogger logger, object message) => logger.Log(LogLevel.Info, message);

		/// <summary>Report diagnostic informations, only in debug builds.</summary>
		[Conditional("DEBUG")]
		public static void Debug(this ILogger logger, object message) => logger.Log(LogLevel.Debug, message);

		/// <summary>Report tracing informations, only in trace builds.</summary>
		[Conditional("TRACE")]
		public static void Trace(this ILogger logger, object message) => logger.Log(LogLevel.Trace, message);
	}
}
