using System.Diagnostics;

namespace GameEngine.Diagnostics.Internal
{
	static class StackWalker
	{
		/// <summary>
		/// Get source line (if unavailable, method name) of this function's caller outside this assembly.
		/// </summary>
		/// <returns></returns>
		public static string GetExternalCallerLocation()
		{
			var loggingAssembly = typeof(StackWalker).Assembly;
			var stacktrace = new StackTrace();
			var frameIndex = 0;

			while (stacktrace.FrameCount > frameIndex)
			{
				var frame = stacktrace.GetFrame(frameIndex++);
				var method = frame.GetMethod();
				if (method.ReflectedType.Assembly != loggingAssembly)
				{
					// read source line of the caller
					var filename = frame.GetFileName();
					var line = frame.GetFileLineNumber();

					// file + line are not always available
					if (filename != null && line != 0)
						return $"{filename}:{line}";

					// if file + line is not available, we rely on the name of the method

					// for interface methods `source.Name` is the full interface name + method name
					// we look for such situations by searching for a dot in the name, except if
					// at index zero, which is the ".ctor" case
					if (method.Name.IndexOf('.') >= 1)
						return method.Name;

					return $"{method.DeclaringType.FullName}.{method.Name}";
				}
			}

			return null;
		}
	}
}
