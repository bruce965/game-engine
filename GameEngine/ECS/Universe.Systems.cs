using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using GameEngine.Diagnostics.Logging;

namespace GameEngine.ECS
{
	partial interface IReadOnlyUniverse { }

	partial interface IUniverse
	{
		/// <summary>Systems registered in this universe.</summary>
		IReadOnlyList<ISystem> Systems { get; }

		/// <summary>Register a system in the execution queue of this universe.</summary>
		/// <param name="order">When should the system be executed relative to <see cref="ExecutionOrder.Current"/>.</param>
		void RegisterSystem(ISystem system, ExecutionOrder order);

		/// <summary>Register a processor in this universe.</summary>
		void RegisterProcessor(IProcessor processor);

		/// <summary>
		/// Build the execution plan for this universe.
		/// <remarks>This method should only be called once per-universe.</remarks>
		/// </summary>
		void Build();

		/// <summary>
		/// Execute once all the systems registered in this universe.
		/// <remarks>This method should only be called after building the universe throught the <see cref="Build"/> method.</remarks>
		/// </summary>
		void Execute();
	}

	partial interface IUniverseInternal { }

	partial class Universe
	{
		Dictionary<ExecutionOrder, List<ISystem>> systems = new Dictionary<ExecutionOrder, List<ISystem>>();
		HashSet<IProcessor> processors = new HashSet<IProcessor>();

		Action execute;
		bool isBuilt => execute != null;

		public IReadOnlyList<ISystem> Systems { get; set; }

		public void Build()
		{
			if (DiagnosticsEnabled)
			{
				if (isBuilt)
					Logger.Error($"Trying to build an already built universe. {nameof(Build)} should only be called once.");
			}

			// configure all processors
			foreach (var processor in processors)
			{
				processor.Initialize(this);
			}

			// configure all systems
			var relations = new Dictionary<ISystem, SystemRelations>();
			foreach (var system in systems.OrderBy(b => b.Key).SelectMany(b => b.Value))
			{
				var systemRelations = new SystemRelations(this);
				system.Initialize(systemRelations);

				relations.Add(system, systemRelations);
			}

			// assign local fields
			var systemsFlattened = systems.OrderBy(kvp => kvp.Key).SelectMany(kvp => kvp.Value).ToArray();
			this.Systems = systemsFlattened;

			// build execution plan
			if (DiagnosticsEnabled)
			{
				diagnosticsData.Systems.RegisterSystems(Systems);

				long lastExecutionEnd = Stopwatch.GetTimestamp();

				execute = () =>
				{
					var now = Stopwatch.GetTimestamp();
					var lastTimestamp = now;

					// TODO: optimize through parallelization when possible
					foreach (var system in systemsFlattened)
					{
						var systemDiagnostics = diagnosticsData.Systems[system];

						try
						{
							system.Execute();
						}
						catch (Exception e)
						{
							Logger.Error(e);
						}

						now = Stopwatch.GetTimestamp();
						var duration = now - lastTimestamp;
						lastTimestamp = now;

						systemDiagnostics.RegisterExecution(TimeSpan.FromSeconds((double)duration / (double)Stopwatch.Frequency));
					}

					// compute last execution time
					diagnosticsData.LastExecutionTime = TimeSpan.FromSeconds((double)(now - lastExecutionEnd) / (double)Stopwatch.Frequency);

					lastExecutionEnd = now;
				};
			}
			else
			{
				execute = () =>
				{
					// TODO: optimize through parallelization when possible
					foreach (var system in systemsFlattened)
					{
						try
						{
							system.Execute();
						}
						catch (Exception)
						{
							// swallow exceptions because diagnostics are disabled
						}
					}
				};
			}

			// let GC collect unused stuff
			systems = null;
			processors = null;
		}

		public void Execute()
		{
			if (DiagnosticsEnabled)
			{
				if (!isBuilt)
					Logger.Error($"Trying to execute an unbuilt universe. Call {nameof(Build)} before calling ${nameof(Execute)}.");
			}

			execute();
		}

		public void RegisterSystem(ISystem system, ExecutionOrder order)
		{
			if (DiagnosticsEnabled)
			{
				if (isBuilt)
					Logger.Warning($"Trying to register a system in built universe. Move {nameof(RegisterSystem)} before ${nameof(Build)}.");
			}

			if (!systems.TryGetValue(order, out var positionBucket))
				systems[order] = positionBucket = new List<ISystem>();

			if (order >= ExecutionOrder.Current)
			{
				// if system is being registered after current position, we add it at the end of the position bucket...
				positionBucket.Add(system);
			}
			else
			{
				// ...else if being registered before current position, we add it at the beginning of the position bucket
				positionBucket.Insert(0, system);
			}
		}

		public void RegisterProcessor(IProcessor processor)
		{
			if (DiagnosticsEnabled)
			{
				if (isBuilt)
					Logger.Warning($"Trying to register a processor in built universe. Move {nameof(RegisterProcessor)} before ${nameof(Build)}.");
			}

			processors.Add(processor);
		}
	}

	/// <summary>Keeps track of which components a system reads from and writes to.</summary>
	class SystemRelations : ISystemInitializer
	{
		internal HashSet<IReadOnlyComponent> ReadsFrom = new HashSet<IReadOnlyComponent>();
		internal HashSet<IComponent> WritesTo = new HashSet<IComponent>();
		internal HashSet<Type> ReadsSharedData = new HashSet<Type>();
		internal HashSet<Type> ModifiesSharedData = new HashSet<Type>();
		internal bool ReadsFromUniverse;
		internal bool WritesToUniverse;

		readonly IUniverse universe;

		public SystemRelations(IUniverse universe) => this.universe = universe;

		IUniverse ISystemInitializer.WritesToUniverse()
		{
			ReadsFromUniverse = true;
			WritesToUniverse = true;
			return universe;
		}

		IReadOnlyUniverse ISystemInitializer.ReadsFromUniverse()
		{
			ReadsFromUniverse = true;
			return universe;
		}

		IReadOnlyComponent ISystemInitializer.ReadsFrom(Type componentType)
		{
			var component = universe.GetComponent(componentType);
			ReadsFrom.Add(component);
			return component;
		}

		IComponent ISystemInitializer.WritesTo(Type componentType)
		{
			var component = universe.GetComponent(componentType);
			WritesTo.Add(component);
			return component;
		}

		T ISystemInitializer.ReadsSharedData<T>()
		{
			ReadsSharedData.Add(typeof(T));
			return universe.GetSharedData<T>();
		}

		T ISystemInitializer.ModifiesSharedData<T>()
		{
			ModifiesSharedData.Add(typeof(T));
			return universe.GetSharedData<T>();
		}
	}
}
