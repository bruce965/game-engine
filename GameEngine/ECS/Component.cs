using System;
using System.Collections.Generic;
using System.Linq;

namespace GameEngine.ECS
{
	/// <summary>Read-only <see cref="IComponent"/>.</summary>
	public interface IReadOnlyComponent
	{
		Type Type { get; }
		IEnumerable<Type> Dependencies { get; }
		int Count { get; }
		bool EntityHas(long entityId);
		IEnumerable<long> GetEntityIds();
	}

	/// <summary>Typed read-only <see cref="IComponent"/>.</summary>
	/// <typeparam name="TData">Type of the data inside this component.</typeparam>
	public interface IReadOnlyComponent<TData> : IReadOnlyComponent where TData : struct
	{
		ref readonly EntityComponent<TData> TryGetFromEntity(long entityId, out bool found);
		ReadOnlySpan<EntityComponent<TData>> ToSpan();
	}

	/// <summary>Piece of data associated with an entity.</summary>
	public interface IComponent : IReadOnlyComponent
	{
		void AddToEntity(long entityId);
		bool RemoveFromEntity(long entityId);
	}

	/// <summary>Typed <see cref="IComponent"/>.</summary>
	/// <typeparam name="TData">Type of the data inside this component.</typeparam>
	public interface IComponent<TData> : IComponent, IReadOnlyComponent<TData> where TData : struct
	{
		new ref EntityComponent<TData> AddToEntity(long entityId);
		new ref EntityComponent<TData> TryGetFromEntity(long entityId, out bool found);
		bool Remove(ref EntityComponent<TData> component);
		new Span<EntityComponent<TData>> ToSpan();
	}

	/// <summary><see cref="IComponent"/> with data of type <typeparamref name="TData"/>, with no more than one instance per-entity.</summary>
	/// <typeparam name="TData">Type of the data inside this component.</typeparam>
	public sealed class Component<TData> : IComponent<TData> where TData : struct
	{
		/// <summary>Fake, used for optimization.</summary>
		EntityComponent<TData> fake;

		/// <summary>Buffer for components added to this storage.</summary>
		EntityComponent<TData>[] data = new EntityComponent<TData>[32];

		/// <summary>Number of assigned components in <see cref="data"/>.</summary>
		int assignedCount = 0;

		/// <summary>Entity id to index of the component inside <see cref="data"/>.</summary>
		readonly Dictionary<long, int> entityIdToIndex = new Dictionary<long, int>();

		Type IReadOnlyComponent.Type => typeof(TData);

		IEnumerable<Type> IReadOnlyComponent.Dependencies => Dependencies;
		ISet<Type> Dependencies { get; } = new HashSet<Type>(
			typeof(TData)
				.GetCustomAttributes(typeof(RequiresComponentAttribute), true)
				.Cast<RequiresComponentAttribute>()
				.Select(attribute => attribute.ComponentType)
		);

		public int Count => assignedCount;

		public ref EntityComponent<TData> AddToEntity(long entityId)
		{
			entityIdToIndex.Add(entityId, assignedCount);

			// if array is completely full, double it's size
			if (assignedCount >= data.Length)
				Array.Resize(ref data, data.Length * 2);

			// add component at the end
			ref var component = ref data[assignedCount++];
			component = new EntityComponent<TData>(entityId);

			return ref component;
		}
		void IComponent.AddToEntity(long entityId) => AddToEntity(entityId);

		public ref EntityComponent<TData> TryGetFromEntity(long entityId, out bool found)
		{
			if (!entityIdToIndex.TryGetValue(entityId, out var index))
			{
				fake = default(EntityComponent<TData>);
				found = false;
				return ref fake;
			}

			found = true;
			return ref data[index];
		}

		public bool EntityHas(long entityId) => entityIdToIndex.ContainsKey(entityId);

		public bool RemoveFromEntity(long entityId)
		{
			if (!entityIdToIndex.TryGetValue(entityId, out var index))
				return false;

			entityIdToIndex.Remove(entityId);

			// if this is the only component in the collection, just remove it
			if (assignedCount == 1)
			{
				assignedCount = 0;
				return true;
			}

			// replace this component with the last component added to the collection and remove the last component
			data[index] = data[assignedCount - 1];
			entityIdToIndex[data[assignedCount].EntityId] = index;
			assignedCount--;

			return true;
		}

		public bool Remove(ref EntityComponent<TData> component) => RemoveFromEntity(component.EntityId);

		public IEnumerable<long> GetEntityIds() => entityIdToIndex.Keys;

		public Span<EntityComponent<TData>> ToSpan() => new Span<EntityComponent<TData>>(data, 0, assignedCount);

		ref readonly EntityComponent<TData> IReadOnlyComponent<TData>.TryGetFromEntity(long entityId, out bool found) => ref TryGetFromEntity(entityId, out found);

		ReadOnlySpan<EntityComponent<TData>> IReadOnlyComponent<TData>.ToSpan() => new ReadOnlySpan<EntityComponent<TData>>(data, 0, assignedCount);
	}
}
