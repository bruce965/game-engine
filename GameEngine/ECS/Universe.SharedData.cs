using System;
using System.Collections.Generic;
using GameEngine.Diagnostics.Logging;

namespace GameEngine.ECS
{
	partial interface IReadOnlyUniverse { }

	partial interface IUniverse
	{
		/// <summary>
		/// Get a chunk of data shared between all systems in the universe.
		/// <remarks>
		/// This method must be called multiple times for the same object if the object should be exposed as multiple types.
		/// Shared objects should not expose any data shared with other shared objects.
		/// </remarks>
		/// </summary>
		/// <typeparam name="T">Type of the shared data. Using the same type will return the same data to all systems.</typeparam>
		void RegisterSharedData<T>(T sharedData) where T : class;

		/// <summary>Retrieve a chunk of shared data.</summary>
		/// <typeparam name="T">Type of the shared data. Using the same type will return the same data to all systems.</typeparam>
		T GetSharedData<T>() where T : class;
	}

	partial class Universe
	{
		Dictionary<Type, object> sharedData = new Dictionary<Type, object>();

		public void RegisterSharedData<T>(T data) where T : class
		{
			if (DiagnosticsEnabled)
			{
				if (isBuilt)
					Logger.Warning($"Trying to register shared data of type '{typeof(T).FullName}' in built universe. Move {nameof(RegisterSharedData)} before ${nameof(Build)}.");

				if (sharedData.ContainsKey(typeof(T)))
					Logger.Warning($"Trying to register shared data of type '{typeof(T).FullName}' twice. Previous data will be replaced.");
			}

			sharedData[typeof(T)] = data;
		}

		public T GetSharedData<T>() where T : class
		{
			if (!sharedData.TryGetValue(typeof(T), out var data))
			{
				if (DiagnosticsEnabled)
					Logger.Warning($"Trying to retrieve shared data of type '{typeof(T).FullName}' which is not registered. The exact type should be registered, registering the same object multiple times for different types is supported.");

				return default(T);
			}

			return (T)data;
		}
	}
}
