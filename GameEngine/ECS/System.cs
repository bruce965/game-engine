using System;

namespace GameEngine.ECS
{
	/// <summary>Algorithm capable of interacting with <see cref="IComponent"/> in <see cref="Entity"/>.</summary>
	public interface ISystem
	{
		void Initialize(ISystemInitializer system);
		void Execute();
	}

	/// <summary>Initializer used to define the behavior of systems.</summary>
	public interface ISystemInitializer
	{
		/// <summary>
		/// Allow this system to create and destroy entities.
		/// The system will never run in parallel with other systems.
		/// </summary>
		IUniverse WritesToUniverse();

		/// <summary>
		/// Allow this system to read from all entitites.
		/// The system will only run after previous writes to the universe have been completed.
		/// </summary>
		IReadOnlyUniverse ReadsFromUniverse();

		/// <summary>
		/// Allow this system to modify the specified components and add new components of that type.
		/// The system will only run after previous writes to the specified components have been completed.
		/// </summary>
		IComponent WritesTo(Type componentType);

		/// <summary>
		/// Allow this sytem to read from the specified components and destroy components of that type.static
		/// The system will only run after previous writes to the specified components have been completed.
		/// </summary>
		IReadOnlyComponent ReadsFrom(Type componentType);

		/// <summary>
		/// Allow this system to read from a chunk of data shared between all systems in the universe.
		/// The system will only run after previous modification to the same shared object have been completed.
		/// <remarks>
		/// Shared data must not modified by this system.
		/// Use <see cref="ModifiesSharedData"/> if you need to modify shared data from this system.
		/// If shared data is thread-safe and tolerates parallel interaction from multiple systems, using <see cref="ModifiesSharedData"/> might not be necessary.
		/// </remarks>
		/// </summary>
		/// <typeparam name="T">Type of the shared data. Using the same type will return the same data to all systems.</typeparam>
		T ReadsSharedData<T>() where T : class;

		/// <summary>
		/// Allow this system to read and modify a chunk of data shared between all systems in the universe.
		/// The system will only run after previous modification to the same shared object have been completed.
		/// </summary>
		/// <typeparam name="T">Type of the shared data. Using the same type will return the same data to all systems.</typeparam>
		T ModifiesSharedData<T>() where T : class;
	}
}
