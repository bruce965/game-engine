using System.Collections.Generic;

namespace GameEngine.ECS
{
	/// <summary>Reference to an entity.</summary>
	public struct Entity
	{
		internal readonly IUniverseInternal _universe;

		public IUniverse Universe => _universe;
		public readonly long Id;

		internal Entity(IUniverseInternal universe, long id)
		{
			_universe = universe;
			Id = id;
		}

		public ref EntityComponent<TData> AddComponent<TData>() where TData : struct => ref _universe.AddComponentToEntity<TData>(Id);

		public ref EntityComponent<TData> GetComponent<TData>(out bool found) where TData : struct => ref _universe.GetComponentFromEntity<TData>(Id, out found);

		public IEnumerable<IComponent> GetComponents() => _universe.GetComponentsFromEntity(Id);

		public bool RemoveComponent<TData>() where TData : struct => _universe.RemoveComponentFromEntity<TData>(Id);

		public void Destroy() => _universe.DestroyEntity(Id);
	}
}
