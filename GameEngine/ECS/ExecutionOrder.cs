using System;

namespace GameEngine.ECS
{
	/// <summary>
	/// Execution order of a system.
	/// </summary>
	public enum ExecutionOrder : int
	{
		/// <summary>Before first system in the execution sequence.</summary>
		First = 100000,

		/// <summary>Natural position in the execution sequence (default).</summary>
		Current = 200000,

		/// <summary>After last system in the execution sequence.</summary>
		Last = 300000
	}
}
