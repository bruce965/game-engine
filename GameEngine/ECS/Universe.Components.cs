using System;
using System.Collections.Generic;
using System.Linq;
using GameEngine.Diagnostics.Logging;

namespace GameEngine.ECS
{
	partial interface IReadOnlyUniverse
	{
		/// <summary>Components registered in this universe.</summary>
		IReadOnlyCollection<IComponent> Components { get; }

		/// <summary>Get a <see cref="IReadOnlyComponent"> registered in this universe.</summary>
		IReadOnlyComponent<TData> GetComponent<TData>() where TData : struct;

		/// <summary>Get a <see cref="IReadOnlyComponent"> registered in this universe.</summary>
		IReadOnlyComponent GetComponent(Type componentType);
	}

	partial interface IUniverse
	{
		/// <summary>Register a <paramref name="component"/>, allowing this universe to interact with it.</summary>
		void RegisterComponent(IComponent component);

		/// <summary>Get a <see cref="IComponent"> registered in this universe.</summary>
		new IComponent<TData> GetComponent<TData>() where TData : struct;

		/// <summary>Get a <see cref="IComponent"> registered in this universe.</summary>
		new IComponent GetComponent(Type componentType);
	}

	partial interface IUniverseInternal
	{
		ref EntityComponent<TData> AddComponentToEntity<TData>(long entityId) where TData : struct;
		ref EntityComponent<TData> GetComponentFromEntity<TData>(long entityId, out bool found) where TData : struct;
		IEnumerable<IComponent> GetComponentsFromEntity(long entityId);
		bool RemoveComponentFromEntity<T>(long entityId) where T : struct;
	}

	partial class Universe
	{
		readonly Dictionary<Type, IComponent> typeToComponent = new Dictionary<Type, IComponent>();

		public IReadOnlyCollection<IComponent> Components => typeToComponent.Values;

		public void RegisterComponent(IComponent component) => typeToComponent.Add(component.Type, component);

		public IComponent<TData> GetComponent<TData>() where TData : struct => (IComponent<TData>)GetComponent(typeof(TData));

		public IComponent GetComponent(Type componentType)
		{
			if (!typeToComponent.TryGetValue(componentType, out var component))
			{
				if (DiagnosticsEnabled)
				{
					Logger.Error($"Trying to retrieve an unregistered component '{componentType.FullName}'. The component should be registered in the universe.");
				}
			}

			return component;
		}

		IReadOnlyComponent<TData> IReadOnlyUniverse.GetComponent<TData>() => GetComponent<TData>();
		IReadOnlyComponent IReadOnlyUniverse.GetComponent(Type componentType) => GetComponent(componentType);

		ref EntityComponent<TData> IUniverseInternal.AddComponentToEntity<TData>(long entityId)
		{
			var component = GetComponent<TData>();
			if (DiagnosticsEnabled)
			{
				Logger.Trace($"Adding '{typeof(TData).FullName}' to entity #{entityId}.");

				foreach (var dependency in component.Dependencies)
				{
					if (!GetComponent(dependency).EntityHas(entityId))
					{
						Logger.Warning($"Added '{typeof(TData).FullName}' component (which requires '{dependency.FullName}' component) to entity #{entityId}. The required component should be added before adding '{typeof(TData).FullName}'.");
						break;
					}
				}
			}

			return ref component.AddToEntity(entityId);
		}

		ref EntityComponent<TData> IUniverseInternal.GetComponentFromEntity<TData>(long entityId, out bool found) => ref GetComponent<TData>().TryGetFromEntity(entityId, out found);

		IEnumerable<IComponent> IUniverseInternal.GetComponentsFromEntity(long entityId) => typeToComponent.Values.Where(component => component.EntityHas(entityId));

		bool IUniverseInternal.RemoveComponentFromEntity<TData>(long entityId)
		{
			var component = GetComponent<TData>();
			var removed = component.RemoveFromEntity(entityId);

			if (DiagnosticsEnabled)
			{
				var entity = GetEntity(entityId);
				if (!entity.HasComponent<TData>())
				{
					var dependents = entity.GetComponents().Where(c => c.Dependencies.Contains(typeof(TData)));

					if (dependents.Any())
						Logger.Warning($"Removing '{typeof(TData).FullName}' component (which is required by '{dependents.First().Type.FullName}' component) from entity #{entityId}. The dependent component should be removed before removing '{typeof(TData).FullName}'.");
				}

				Logger.Trace($"Removed '{typeof(TData).FullName}' from entity #{entityId}.");
			}

			return removed;
		}
	}
}
