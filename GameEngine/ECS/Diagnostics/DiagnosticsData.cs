﻿using System;

namespace GameEngine.ECS.Diagnostics
{
	/// <summary>
	/// Memory and timing informations.
	/// </summary>
	public interface IDiagnosticsData
	{
		/// <summary>Retrieves the number of bytes currently thought to be allocated to managed resources.</summary>
		long ManagedMemory { get; }

		/// <summary>Diagnostic informations about systems in the universe.</summary>
		ISystemsDiagnostics Systems { get; }

		/// <summary>How long passed between the end of 2nd-last execution and the end of last execution.</summary>
		TimeSpan LastExecutionTime { get; }

		/// <summary>Unsmoothed framerate in frames-per-second.</summary>
		double FPS { get; }
	}

	internal class DiagnosticsData : IDiagnosticsData
	{
		public long ManagedMemory => GC.GetTotalMemory(false);

		public SystemsDiagnostics Systems { get; } = new SystemsDiagnostics();
		ISystemsDiagnostics IDiagnosticsData.Systems => Systems;

		public TimeSpan LastExecutionTime { get; set; }

		public double FPS => 1d / LastExecutionTime.TotalSeconds;
	}
}
