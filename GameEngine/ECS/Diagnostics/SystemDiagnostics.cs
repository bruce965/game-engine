using System;

namespace GameEngine.ECS.Diagnostics
{
	/// <summary>
	/// Diagnostic informations about a system.
	/// </summary>
	public interface ISystemDiagnostics
	{
		/// <summary>Number of times this system has been executed.</summary>
		int ExecutionsCount { get; }

		/// <summary>Total time spent executing this system since the start of the simulation.</summary>
		TimeSpan TotalExecutionTime { get; }

		/// <summary>Time spent executing this system during last execution.</summary>
		TimeSpan LastExecutionTime { get; }
	}

	internal class SystemDiagnostics : ISystemDiagnostics
	{
		public int ExecutionsCount { get; set; }

		public TimeSpan TotalExecutionTime { get; set; }

		public TimeSpan LastExecutionTime { get; set; }

		public void RegisterExecution(TimeSpan duration)
		{
			ExecutionsCount++;
			LastExecutionTime = duration;
			TotalExecutionTime += duration;
		}
	}
}
