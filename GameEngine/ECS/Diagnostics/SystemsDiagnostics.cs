using System.Collections.Generic;

namespace GameEngine.ECS.Diagnostics
{
	/// <summary>
	/// Diagnostic informations about systems in the universe.
	/// </summary>
	public interface ISystemsDiagnostics
	{
		ISystemDiagnostics this[ISystem system] { get; }
	}

	internal class SystemsDiagnostics : ISystemsDiagnostics
	{
		readonly Dictionary<ISystem, SystemDiagnostics> dictionary = new Dictionary<ISystem, SystemDiagnostics>();
		readonly SystemDiagnostics notFound = new SystemDiagnostics();

		ISystemDiagnostics ISystemsDiagnostics.this[ISystem system] => this[system];

		public SystemDiagnostics this[ISystem system]
		{
			get
			{
				if (!dictionary.TryGetValue(system, out var diagnostics))
					return notFound;

				return diagnostics;
			}
		}

		public void RegisterSystems(IEnumerable<ISystem> systems)
		{
			foreach (var system in systems)
			{
				dictionary.Add(system, new SystemDiagnostics());
			}
		}
	}
}
