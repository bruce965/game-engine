using System;

namespace GameEngine.ECS
{
	[AttributeUsage(AttributeTargets.Struct, AllowMultiple = true)]
	public sealed class RequiresComponentAttribute : Attribute
	{
		public Type ComponentType { get; }

		public RequiresComponentAttribute(Type componentType)
		{
			this.ComponentType = componentType;
		}
	}
}
