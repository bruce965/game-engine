using System;

namespace GameEngine.ECS
{
	public struct EntityComponent<TData>
	{
		public readonly long EntityId;
		public TData Data;

		internal EntityComponent(long entityId)
		{
			EntityId = entityId;
			Data = default(TData);
		}
	}
}
