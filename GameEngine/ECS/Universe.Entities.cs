using System.Collections.Generic;
using System.Linq;
using GameEngine.Diagnostics.Logging;

namespace GameEngine.ECS
{
	partial interface IReadOnlyUniverse { }

	partial interface IUniverse
	{
		/// <summary>Get all entities in this universe (very slow operation, only use for debugging purposes).</summary>
		IEnumerable<Entity> Entities { get; }

		/// <summary>Create a new entity; will not be added to the universe until at least one <see cref="IComponent"/> is added to the entity.</summary>
		Entity CreateEntity();

		/// <summary>Get an entity in this universe by id, or create it if it does not exist.</summary>
		Entity GetEntity(long entityId);

		/// <summary>Destroy an entity from this universe (slow operation, destroying the entity by removing all its components is preferred).</summary>
		void DestroyEntity(long entityId);
	}

	partial interface IUniverseInternal { }

	partial class Universe
	{
		// NOTE: assigned by the client; other peers might assign a different id to the same synchronized entity.
		long nextEntityId = 1;

		public IEnumerable<Entity> Entities => Components  // get all components
			.SelectMany(component => component.GetEntityIds())  // get all entity ids from components
			.Distinct()  // remove duplicates (for entities with more than a single component)
			.Select(GetEntity);  // convert ids to entities

		public Entity CreateEntity()
		{
			if (DiagnosticsEnabled)
				Logger.Trace($"Creating entity #{nextEntityId}...");

			return new Entity(this, nextEntityId++);
		}

		public Entity GetEntity(long entityId)
		{
			return new Entity(this, entityId);
		}

		public void DestroyEntity(long entityId)
		{
			if (DiagnosticsEnabled)
				Logger.Trace($"Destroying entity #{entityId}...");

			foreach (var component in typeToComponent.Values)
				component.RemoveFromEntity(entityId);
		}
	}
}
