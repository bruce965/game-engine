using GameEngine.Diagnostics.Logging;
using GameEngine.ECS.Diagnostics;

namespace GameEngine.ECS
{
	/// <summary>Tracks all the entities and components in a simulated universe.</summary>
	public partial interface IReadOnlyUniverse
	{
		/// <summary>Log associated with this universe.</summary>
		ILogger Logger { get; }

		/// <summary>Check for configuration and runtime errors, should always be enabled in development.</summary>
		bool DiagnosticsEnabled { get; }
	}

	/// <summary>Tracks all the entities and components in a simulated universe.</summary>
	public partial interface IUniverse : IReadOnlyUniverse { }

	/// <summary>Methods which are used internally in this library, not part of the public API.</summary>
	internal partial interface IUniverseInternal : IUniverse { }

	/// <summary>Tracks all the entities and components in a simulated universe.</summary>
	public sealed partial class Universe : IUniverse, IUniverseInternal
	{
		public ILogger Logger { get; }

		public bool DiagnosticsEnabled { get; }

		readonly DiagnosticsData diagnosticsData;

		Universe(ILogger logger, bool enableDiagnostics)
		{
			this.Logger = logger ?? NullLogger.Instance;
			this.DiagnosticsEnabled = enableDiagnostics;

			if (DiagnosticsEnabled)
			{
				diagnosticsData = new DiagnosticsData();
				this.RegisterSharedData<IDiagnosticsData>(diagnosticsData);  // public API
				this.RegisterSharedData<DiagnosticsData>(diagnosticsData);  // internal API
			}

			if (DiagnosticsEnabled)
				logger.Trace("Universe created.");
		}

		/// <summary>Create a new universe.</summary>
		/// <param name="enableDiagnostics">Check for configuration and runtime errors. Will incur a slight performance penalty, should always be enabled in development.</param>
		public static IUniverse Create(ILogger logger, bool enableDiagnostics) => new Universe(logger, enableDiagnostics);
	}
}
