using System.Collections.Generic;

namespace GameEngine.ECS
{
	public static class Extensions
	{
		#region Universe Extensions

		public static void RegisterComponent<TData>(this IUniverse universe) where TData : struct
		{
			universe.RegisterComponent(new Component<TData>());
		}

		public static void RegisterSystem(this IUniverse universe, ISystem system)
		{
			universe.RegisterSystem(system, ExecutionOrder.Current);
		}

		public static void RegisterSystem<TSystem>(this IUniverse universe) where TSystem : ISystem, new() => RegisterSystem(universe, new TSystem());

		public static void RegisterProcessor<TProcessor>(this IUniverse universe) where TProcessor : IProcessor, new()
		{
			universe.RegisterProcessor(new TProcessor());
		}

		#endregion

		#region Entity Extensions

		public static bool HasComponent<TData>(this Entity entity) where TData : struct
		{
			entity.GetComponent<TData>(out var found);
			return found;
		}

		public static ref EntityComponent<TData> AddComponent<TData>(this Entity entity, TData data) where TData : struct
		{
			ref var entityComponent = ref entity.AddComponent<TData>();
			entityComponent.Data = data;

			return ref entityComponent;
		}

		#endregion

		#region System Extensions

		/// <summary>Allow this system to modify <typeparamref name="TData"/> components and add new components of that type; only run after previous writes to <typeparamref name="TData"/> components have been completed.</summary>
		public static IComponent<TData> WritesTo<TData>(this ISystemInitializer system) where TData : struct
		{
			return (IComponent<TData>)system.WritesTo(typeof(TData));
		}

		/// <summary>Allow this sytem to read from <typeparamref name="TData"/> components and destroy components of that type; only run after previous writes to <typeparamref name="TData"/> components have been completed.</summary>
		public static IReadOnlyComponent<TData> ReadsFrom<TData>(this ISystemInitializer system) where TData : struct
		{
			return (IReadOnlyComponent<TData>)system.ReadsFrom(typeof(TData));
		}

		/// <summary>Prevent any other system to run in parallel with this.</summary>
		public static void RunsSynchronously(this ISystemInitializer system) => system.WritesToUniverse();

		#endregion

		#region Component Extensions

		public static ref EntityComponent<TData> GetFromEntity<TData>(this IComponent<TData> component, long entityId) where TData : struct
		{
			// HACK: using a stack variable for `found` is not allowed because such variable could be exposed
			// into `entityComponent` by `TryGetFromEntity` method. It works if we use a heap variable.
			var found = new bool[1];

			ref var entityComponent = ref component.TryGetFromEntity(entityId, out found[0]);
			if (!found[0])
			{
				// TODO: logger.Error($"Requesting '{typeof(TData).FullName}' component from entity #{entityId} which does not have it. Ensure that the component will be present or use {nameof(component.TryGetFromEntity)} in place of {nameof(GetFromEntity)}.");
			}

			return ref entityComponent;
		}

		public static ref readonly EntityComponent<TData> GetFromEntity<TData>(this IReadOnlyComponent<TData> component, long entityId) where TData : struct
		{
			// HACK: using a stack variable for `found` is not allowed because such variable could be exposed
			// into `entityComponent` by `TryGetFromEntity` method. It works if we use a heap variable.
			var found = new bool[1];

			ref readonly var entityComponent = ref component.TryGetFromEntity(entityId, out found[0]);
			if (!found[0])
			{
				// TODO: logger.Error($"Requesting '{typeof(TData).FullName}' component from entity #{entityId} which does not have it. Ensure that the component will be present or use {nameof(component.TryGetFromEntity)} in place of {nameof(GetFromEntity)}.");
			}

			return ref entityComponent;
		}

		#endregion
	}
}
