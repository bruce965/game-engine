using GameEngine.Diagnostics.Logging;

namespace GameEngine.ECS
{
	/// <summary>Hooks to the universe and adds functionality.</summary>
	public interface IProcessor
	{
		/// <summary>Called when the universe is built, before initializing systems.</summary>
		void Initialize(IUniverse universe);
	}
}
