using System;

namespace GameEngine.Network
{
	/// <summary>
	/// Provides mean to transfer packets over the network.
	/// </summary>
	public interface ISocket
	{
		/// <summary>
		/// Id of the owner of this socket, <c>0</c> if owner is unknown or owner is the server.
		/// <seealso cref="NetworkEntity.PeerId"/>
		/// </summary>
		long PeerId { get; }

		/// <summary>Send a message to the specified target.</summary>
		/// <param name="targetId"><see cref="PeerId"/> of the target, <c>0</c> to broadcast.</param>
		/// <param name="data">Data in the message.</param>
		/// <param name="packetType">How should the packet be transferred.</param>
		/// <remarks>Not thread safe, assumes a single producer.</remarks>
		void Send(long targetId, Span<byte> data, PacketType packetType);

		/// <summary>Receive a message, does not block.</summary>
		/// <param name="sourceId"><see cref="PeerId"/> of the sender.</param>
		/// <param name="data">Data in the message.</param>
		/// <returns><c>true</c> if a message has been received, <c>false</c> otherwise.</returns>
		/// <remarks>Not thread safe, assumes a single consumer.</remarks>
		bool Receive(out long sourceId, out Memory<byte> data);
	}
}
