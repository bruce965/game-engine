using System;

namespace GameEngine.Network
{
	/// <summary>
	/// Defines if a component should be synchronized over the network.
	/// </summary>
	[AttributeUsage(AttributeTargets.Struct, AllowMultiple = false)]
	public sealed class SynchronizeAttribute : Attribute
	{
		/// <summary>Defines which peer to synchronize with.</summary>
		public SyncDirection Direction { get; }

		/// <summary>Defines when to automatically synchronize.</summary>
		public SyncStrategy Strategy
		{
			get
			{
				// if a strategy has been specified use it
				if (strategy.HasValue)
					return strategy.Value;

				// else always sync on spawn/destory, and...
				return SyncStrategy.OnSpawn &
					// ...if `Interval` is defined use it, else sync every frame
					(Interval == TimeSpan.Zero ? SyncStrategy.EveryFrame : SyncStrategy.ByInterval);
			}
		}
		readonly SyncStrategy? strategy;

		/// <summary>How often to synchronize (if <see cref="SyncStrategy.ByInterval"/> flag is set).</summary>
		public TimeSpan Interval { get; set; } = TimeSpan.Zero;

		public SynchronizeAttribute(SyncDirection direction, SyncStrategy strategy)
		{
			this.Direction = direction;
			this.strategy = strategy;
		}

		public SynchronizeAttribute(SyncDirection direction)
		{
			this.strategy = null;  // auto-detect strategy based on attributes
		}

		public SynchronizeAttribute() : this(SyncDirection.ServerToEverybody) { }
	}
}
