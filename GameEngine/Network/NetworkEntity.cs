using System;

namespace GameEngine.Network
{
	/// <summary>
	/// Defines which peer owns an entity.
	/// </summary>
	// NOTE: `SyncDirection` does not apply to this special component, direction is determined by other components on the same entity.
	[Synchronize(default(SyncDirection), SyncStrategy.OnSpawn)]
	public struct NetworkEntity
	{
		/// <summary>
		/// Id of this network entity, used to identify this entity during synchronization.
		/// <remarks>This id is assigned from the server, unlike <see cref="GameEngine.ECS.Entity.Id"/> which is assigned from the client.</remarks>
		/// </summary>
		public long EntityId;

		/// <summary>
		/// Id of the owner of this entity, <c>0</c> if no owner, owner is unknown or owner is the server.
		/// </summary>
		public long PeerId;
	}
}
