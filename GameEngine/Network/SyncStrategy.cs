using System;
using GameEngine.ECS;

namespace GameEngine.Network
{
	/// <summary>
	/// Defines when a component should be automatically synchronized over the network.
	/// </summary>
	[Flags]
	public enum SyncStrategy
	{
		/// <summary>Never synchronized automatically.</summary>
		Manually = 0,

		/// <summary>Synchronized when added to the scene or removed from the scene.</summary>
		OnSpawn = 1,

		/// <summary>Synchronized when data changes (forces network system to store a copy of this component and continuously check for changes).</summary>
		OnChange = 2,

		/// <summary>Synchronized every frame, even if unchanged.</summary>
		EveryFrame = 4,

		/// <summary>Synchronized periodically.</summary>
		ByInterval = 8,

		/// <summary>Synchronized when the synchronization flag is set.</summary>
		ByFlag = 16
	}
}
