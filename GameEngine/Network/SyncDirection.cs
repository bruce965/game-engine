using System;

namespace GameEngine.Network
{
	/// <summary>
	/// Defines which peers a component should be synchronized with.
	/// </summary>
	[Flags]
	public enum SyncDirection
	{
		/// <summary>Not synchronized.</summary>
		Nobody = 0,

		/// <summary>Synchronize from server to all clients excluding owner.</summary>
		ServerToOthers = 1,

		/// <summary>Synchronized from server to owner (see <see cref="NetworkEntity.PeerId"/>).</summary>
		ServerToOwner = 2,

		/// <summary>Synchronized from owner to server (see <see cref="NetworkEntity.PeerId"/>).</summary>
		OwnerToServer = 4,

		/// <summary>Synchronized from owner to server, then from server to all clients excluding owner (see <see cref="NetworkEntity.PeerId"/>).</summary>
		OwnerToOthers = OwnerToServer | ServerToOthers,

		/// <summary>Synchronize from server to all clients including owner.</summary>
		ServerToEverybody = ServerToOwner | ServerToOthers,

		/// <summary>Synchronized from owner to server, then from server to all clients including owner (see <see cref="NetworkEntity.PeerId"/>).</summary>
		OwnerToEverybody = OwnerToServer | ServerToEverybody
	}
}
