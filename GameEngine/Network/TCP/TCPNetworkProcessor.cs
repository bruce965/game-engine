using System;
using System.Buffers;
using System.Net.Sockets;
using GameEngine.Utility.Collections;

namespace GameEngine.Network
{
	/// <summary>Installs the <see cref="ISystem"> instances required to synchronize this universe between multiple remote machines through a TCP socket.</summary>
	public sealed class TCPNetworkProcessor : BaseNetworkProcessor, IDisposable
	{
		/// <summary>Handler used to filter incoming connections.</summary>
		/// <param name="client">Incoming connection TCP client.</param>
		/// <param name="peerId"><see cref="ISocket.PeerId"/> assigned to the client if the connection is accepted.</param>
		/// <returns><c>true</c> to accept the connection or <c>false</c> to reject.</returns>
		public delegate bool IncomingConnectionHandler(TcpClient client, long peerId);

		delegate void PacketsHandler(Span<Packet> packets);

		struct Packet
		{
			public long PeerId;
			public ArraySegment<byte> Data;
			public PacketType Type;
		}

		/// <remarks>Not thread safe.</remarks>
		sealed class TCPSocket : ISocket, IDisposable
		{
			long peerId;
			long ISocket.PeerId => peerId;

			readonly PooledList<Packet> receivedPackets = new PooledList<Packet>(ArrayPool<Packet>.Shared, false);
			readonly PooledList<Packet> sendingPackets = new PooledList<Packet>(ArrayPool<Packet>.Shared, false);
			int readerPosition = 0;

			public TCPSocket(long peerId)
			{
				this.peerId = peerId;
			}

			/// <summary>Add packets to the list of incoming packets.</summary>
			/// <remarks>Not thread safe.</remarks>
			public void AddIncoming(long sourceId, ArraySegment<byte> data)
			{
				receivedPackets.Add(new Packet { PeerId = sourceId, Data = data });
			}

			/// <summary>Retrieve and process all outgoing packets, then clear the list.</summary>
			/// <remarks>Not thread safe.</remarks>
			public void FlushOutgoing(PacketsHandler handle)
			{
				handle(sendingPackets.AsSpan());
				sendingPackets.Clear();
			}

			bool ISocket.Receive(out long sourceId, out Memory<byte> data)
			{
				// if we read all the packets in the list of incoming packets, we clear it
				if (readerPosition >= receivedPackets.Count)
				{
					// return to the pool all the buffers for received messages
					foreach (var packet in receivedPackets)
						ArrayPool<byte>.Shared.Return(packet.Data.Array);

					// clear the list
					receivedPackets.Clear();
					readerPosition = 0;

					// report that there are no more packets to be read
					sourceId = 0;
					data = Memory<byte>.Empty;
					return false;
				}

				// return first unread packet
				var lastPacket = receivedPackets[readerPosition++];
				sourceId = lastPacket.PeerId;
				data = lastPacket.Data;
				return true;
			}

			void ISocket.Send(long targetId, Span<byte> data, PacketType packetType)
			{
				// NOTE: `dataBuffer` will be returned to the pool after sending.
				var dataBuffer = ArrayPool<byte>.Shared.Rent(data.Length);
				data.CopyTo(dataBuffer);

				// add packet to the list of outgoing packets
				sendingPackets.Add(new Packet
				{
					PeerId = targetId,
					Data = new ArraySegment<byte>(dataBuffer, 0, data.Length),
					Type = packetType
				});
			}

			void IDisposable.Dispose()
			{
				receivedPackets.ReturnToPool();
			}
		}

		TCPSocket socket;
		long lastPeerId = 0;

		TCPNetworkProcessor(TCPSocket socket, bool isServer) : base(socket, isServer) { }

		public static TCPNetworkProcessor Server(TcpListener server, IncomingConnectionHandler handler)
		{
			var socket = new TCPSocket(0);
			// TODO: start listening and enqueue

			return new TCPNetworkProcessor(socket, true);
		}

		/// <summary>Close the socket permanently.</summary>
		public void Terminate()
		{
			if (socket != null)
			{
				((IDisposable)socket).Dispose();
				socket = null;
			}
		}

		void IDisposable.Dispose() => Terminate();
	}
}
