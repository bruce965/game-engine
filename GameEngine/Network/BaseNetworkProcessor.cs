using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Reflection.Emit;
using GameEngine.Common;
using GameEngine.ECS;

namespace GameEngine.Network
{
	/// <summary>Installs the <see cref="ISystem"> instances required to synchronize this universe between multiple remote machines.</summary>
	public class BaseNetworkProcessor : IProcessor
	{
		protected readonly ISocket Socket;
		protected readonly bool IsServer;

		/// <summary>Build an abstract network processor.</summary>
		/// <param name="socket">Socket used to exchange data.</param>
		/// <param name="isServer">Indicates wether this processor is being installed on a server or a client.</param>
		public BaseNetworkProcessor(ISocket socket, bool isServer)
		{
			this.Socket = socket;
			this.IsServer = isServer;
		}

		void IProcessor.Initialize(IUniverse universe)
		{
			// search for network-enabled components registered in the universe
			var networkEnabledComponents = universe.Components
				.Select<IComponent, (IComponent Component, SynchronizeAttribute Attribute)>(c => (c, (SynchronizeAttribute)c.Type.GetCustomAttributes(typeof(SynchronizeAttribute), false).FirstOrDefault()))
				.Where(x => x.Attribute != null)  // exclude component that don't have the `SynchronizeAttribute`
				.Where(x => x.Attribute.Direction != SyncDirection.Nobody)  // exclude components that are marked as not shared
				.Where(x => x.Attribute.Strategy != SyncStrategy.Manually);  // exclude components that are not marked automatically synchronized

			var (networkStatus, receive, send) = GenerateNetworkSystems(networkEnabledComponents);

			// we use this custom-crafted component to store the synchronization state of other components
			universe.RegisterComponent(networkStatus);

			// run `receive` system before executing "game logic" systems
			universe.RegisterSystem(receive, ExecutionOrder.First);

			// run `send` system after executing "game logic" systems
			universe.RegisterSystem(send, ExecutionOrder.Last);
		}

		/// <summary>
		/// Generate a "NetworkStatus" component and two systems: "Receive" and "Send".
		///
		/// The "Receive" system applies diff patches received from remote client/server and collects initial status of the universe.
		///
		/// The "Send" system compares current status of the universe with the initial and sends diff patches to the remote client/server.
		/// </summary>
		static (IComponent NetworkStatus, ISystem Receive, ISystem Send) GenerateNetworkSystems(
			IEnumerable<(IComponent Component, SynchronizeAttribute Attribute)> networkEnabledComponents
		)
		{
			// keep a full copy of the components that require synchronization on change
			var fullCopyComponents = networkEnabledComponents
				.Where(x => (x.Attribute.Strategy & SyncStrategy.OnChange) != 0);

			// keep time of the last sync of the components that require synchronization by interval
			var byIntervalComponents = networkEnabledComponents
				.Where(x => (x.Attribute.Strategy & SyncStrategy.ByInterval) != 0);

			// prepare IL builder
			var name = $"NetworkProcessor_{Guid.NewGuid().ToString().Replace("-", "")}";
			var assemblyBuilder = AssemblyBuilder.DefineDynamicAssembly(new AssemblyName(name), AssemblyBuilderAccess.Run);
			var moduleBuilder = assemblyBuilder.DefineDynamicModule($"{name}.dll");

			#region NetworkStatus Component

			/*
			 * // NetworkStatus Component
			 *
			 * public struct NetworkStatus
			 * {
			 *     // Full copy of components that require synchronization on change
			 *     public SomeComponent1 SomeComponent1_Data0;
			 *     public SomeComponent2 SomeComponent2_Data1;
			 *     // …
			 *     public SomeComponentN SomeComponentN_DataN;
			 *
			 *     // Time of the next sync of components that require synchronization by interval
			 *     public long SomeComponent1_NextSync0;
			 *     public long SomeComponent2_NextSync1;
			 *     // …
			 *     public long SomeComponentN_NextSyncN;
			 * }
			 */

			var networkStatusBuilder = moduleBuilder.DefineType(
				$"{name}.NetworkStatus",
				TypeAttributes.Public | TypeAttributes.Sealed | TypeAttributes.Serializable,
				typeof(ValueType)  // struct
			);

			// generate data fields for the components that need it
			var i = 0;
			foreach (var component in fullCopyComponents)
			{
				var fieldBuilder = networkStatusBuilder.DefineField(
					$"{component.Component.Type.Name}_Data{i++}",  // `i++` progressive to avoid risk of two fields with same name
					component.Component.Type,
					FieldAttributes.Public
				);
			}

			// generate "next sync" fields for the components that need it
			i = 0;
			foreach (var component in byIntervalComponents)
			{
				var fieldBuilder = networkStatusBuilder.DefineField(
					$"{component.Component.Type.Name}_NextSync{i++}",
					typeof(long),
					FieldAttributes.Public
				);
			}

			#endregion

			#region Receive System

			/*
			 * // Receive System
			 *
			 * public sealed class ReceiveSystem : ISystem<ISharedData>
			 * {
			 *     readonly ISocket socket;
			 *     readonly long peerId;
			 *
			 *     IComponent<SomeComponent1> SomeComponent1_Component0;
			 *     IComponent<SomeComponent2> SomeComponent2_Component1;
			 *     // …
			 *     IComponent<SomeComponentN> SomeComponentN_ComponentN;
			 *
			 *     IComponent<NetworkStatus> NetworkStatus;
			 *
			 *     public ReceiveSystem(ISocket socket, long peerId)
			 *     {
			 *         this.socket = socket;
			 *         this.peerId = peerId;
			 *     }
			 *
			 *     public void Initialize(ISystemInitializer system)
			 *     {
			 *         system.WritesTo(typeof(SomeComponent1));
			 *         system.WritesTo(typeof(SomeComponent2));
			 *         // …
			 *         system.WritesTo(typeof(SomeComponentN));
			 *
			 *         system.WritesTo(typeof(NetworkStatus));
			 *     }
			 *
			 *     public void Execute(ISharedData shared)
			 *     {
			 *         // apply patches received from remote
			 *         // TODO
			 *
			 *         // collect initial universe status
			 *         // TODO
			 *     }
			 * }
			 */

			var receiveSystemBuilder = moduleBuilder.DefineType(
				$"{name}.SendSystem",
				TypeAttributes.Public | TypeAttributes.Sealed,
				typeof(object),
				new[] { typeof(ISystem) }
			);

			// TODO

			#endregion

			#region Send System

			// TODO

			#endregion

			var networkStatusType = networkStatusBuilder.CreateTypeInfo().AsType();

			return (
				(IComponent)Activator.CreateInstance(typeof(Component<>).MakeGenericType(networkStatusType)),
				null,  // TODO
				null  // TODO
			);
		}
	}
}
