using System;

namespace GameEngine.Network
{
	/// <summary>
	/// Marks a boolean field as a flag for network synchronization, when <see cref="SyncStrategy.ByFlag"/> flag is set.
	/// </summary>
	[AttributeUsage(AttributeTargets.Field, AllowMultiple = false)]
	public sealed class SynchronizationFlagAttribute : Attribute
	{
		public SynchronizationFlagAttribute() { }
	}
}
