using System;

namespace GameEngine.Network
{
	/// <summary>
	/// Describes to transfer a packet over the network.
	/// </summary>
	[Flags]
	public enum PacketType
	{
		/// <summary>Packet may be reordered, duplicated or dropped if network is congested.</summary>
		Unreliable = 0,

		/// <summary>Packet may not be dropped if networks is congested, but it meay be reordered or duplicated.</summary>
		Reliable = 1,

		/// <summary>Packet may not be reordered, but it may be duplicated or dropped if network is congested.</summary>
		Sequential = 2,

		/// <summary>Packet may not be duplicated, but it may be reoredered or dropped if network is congested.</summary>
		Single = 4,

		/// <summary>Packet may not be reoredered, duplicated or dropped if network is congested.</summary>
		Stream = Reliable | Sequential | Single
	}
}
