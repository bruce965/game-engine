﻿using System;
using System.Numerics;
using GameEngine.ECS;

namespace GameEngine.Graphics.Graphics2D
{
	public struct Transform2D
	{
		/// <summary>Parent entity of this transform, or <c>0</c> if none.</summary>
		public long ParentId;

		/// <summary>Transform relative to its parent.</summary>
		public Matrix3x2 Transform;

		/// <summary>Get absolute transform.</summary>
		public Matrix3x2 GetAbsoluteTransform(IReadOnlyComponent<Transform2D> component)
		{
			// if it has no parent, we return position directly
			if (ParentId == 0)
				return Transform;

			ref readonly var parent = ref component.TryGetFromEntity(ParentId, out var found);

			// if it had a parent which does not exist anymore, we return position directly
			if (!found)
				return Transform;

			// if it has a parent, return parent's position + this transform's position relative to parent
			return parent.Data.GetAbsoluteTransform(component) * Transform;
		}

		public Transform2D(Vector2 position)
		{
			this.ParentId = 0;
			this.Transform = Matrix3x2.CreateTranslation(position);
		}

		public Transform2D(float x, float y) : this(new Vector2(x, y)) { }
	}
}
