using System;
using System.Numerics;
using GameEngine.ECS;

namespace GameEngine.Graphics.Graphics2D
{
	[RequiresComponent(typeof(Transform2D))]
	public struct Sprite2D
	{
		/// <summary>Size on the screen.</summary>
		public Vector2 Size;

		/// <summary>UV texture coordinates of the top-left corner.</summary>
		public Vector2 UVTopLeft;

		/// <summary>UV texture coordinates of the bottom-right corner.</summary>
		public Vector2 UVBottomRight;

		/// <summary>Texture of this sprite.</summary>
		public ITexture2D Texture;

		public Sprite2D(ITexture2D texture)
		{
			this.Size = new Vector2(32, 32);
			this.UVTopLeft = Vector2.Zero;
			this.UVBottomRight = Vector2.One;
			this.Texture = texture;
		}
	}
}
