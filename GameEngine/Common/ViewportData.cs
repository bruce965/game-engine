using System;

namespace GameEngine.Common
{
	/// <summary>
	/// Informations about the viewport.
	/// </summary>
	public interface IViewportData
	{
		/// <summary>Viewport width in pixels.</summary>
		int Width { get; }

		/// <summary>Viewport height in pixels.</summary>
		int Height { get; }
	}

	/// <summary>
	/// Informations about the viewport.
	/// </summary>
	public class ViewportData : IViewportData
	{
		public int Width { get; set; }
		public int Height { get; set; }
	}
}
