using System;
using System.Numerics;

namespace GameEngine.Common
{
	/// <summary>
	/// Filtered and normalized input data.
	/// </summary>
	public interface IInputData
	{
		/// <summary>Position of the main pointer on the screen.</summary>
		Vector2 Pointer { get; }
	}

	/// <summary>
	/// Filtered and normalized input data.
	/// </summary>
	public class InputData : IInputData
	{
		public Vector2 Pointer { get; set; }
	}
}
