using System;
using System.Numerics;

namespace GameEngine.Common
{
	/// <summary>
	/// Native window data.
	/// </summary>
	public unsafe interface INativeData
	{
		/// <summary>Native window handle.</summary>
		void* WindowHandle { get; }
	}

	/// <summary>
	/// Native window data.
	/// </summary>
	public unsafe class NativeData : INativeData
	{
		public void* WindowHandle { get; set; }
	}
}
