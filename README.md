Game Engine
===========

*WARNING: this is an experimental and incomplete software, name and public API will change.*

Prototype of an high performance .NET Core game engine.

## Important Concepts

### Universe

Provides a structured object-oriented view over each existing `Entity`, `Component` and `System`.

```c#
Universe universe = Universe.Create(…);
```

### Entity

Anything that lives in the `Universe`.

At least one `Component` must be associated with each `Entity`, the `Entity` will cease to exist when it has no `Component` associated.

```c#
Entity player = universe.CreateEntity();
player.AddComponent(new Transform2D(10, 10));
player.AddComponent(new Sprite2D(playerTexture));
```

### Component

Marks an `Entity` as possessing some traits or properties with some values.

In C#, components are simple structures. All the data is referenced from `Component` structures.

It is possible to add get/set properties and methods to components as long as such properties and methods don't access any external resource.

Flags are components with no fields.

```c#
// component for a creature with some health
struct Creature
{
	public float Health;
}

// flags to give traits to entities
struct Friendly { }
struct Aggressive { }

// ensure all dogs also have a Transform2D component
[RequiresComponent(typeof(Transform2D))]
struct DogAI { }

[RequiresComponent(typeof(Transform2D))]
struct FollowPointer { }
```

### System

Executes logic operations over `Component` data. The executable code is inside the `System` class.

A `System` should not access any external data, with the exception of contants and data that does not change while the `System` is running.

If access to external resources is absolutely required, these resources must accept potentially parallel access from multiple `System`s. In alternative, call `system.RunsSynchronously()` to mark the `System` as non-parallelizable with other `System`s.

```c#
// makes entities with the FollowPointer flag follow the pointer
class FollowPointerSystem : ISystem
{
	IReadOnlyComponent<FollowPointer> FollowPointer;
	IComponent<Transform2D> Transform2D;
	IInputData Input;

	public void Initialize(ISystemInitializer system)
	{
		// initialize by specifying what kind of operations this system will perform
		FollowPointer = system.ReadsFrom<FollowPointer>();
		Transform2D = system.WritesTo<Transform2D>();
		Input = system.ReadsSharedData<IInputData>();
	}

	public void Execute()
	{
		// iterate all entities that have the FollowPointer flag
		var controlledByKeyboard = FollowPointer.ToSpan();
		for (var i = 0; i < controlledByKeyboard.Length; i++)
		{
			ref readonly var entityId = ref controlledByKeyboard[i].EntityId;

			// get Transform2D component of the entity and set translation to mouse pointer
			ref var transform = ref Transform2D.GetFromEntity(entityId).Data;
			transform.Transform.Translation = Input.Pointer;
		}
	}
}

universe.RegisterSystem(new FollowPointerSystem());
universe.RegisterSystem(new Sprite2DRenderingSystem());
```

### Processor

An prebuilt "package" that can selectively register required `System` classes and `Component` structures into the `Universe`.

A `Processor` will read the configuration of the `Universe` and register the resources required to run it optimally.

```c#
Universe universe = Universe.Create(…);
universe.RegisterProcessor(new RenderingProcessor());
universe.RegisterComponent<Transform2D>();
universe.RegisterComponent<Sprite2D>();
universe.Build();

// since we register a Sprite2D component in the
// universe, the RenderingProcessor will automatically
// register a Sprite2DRenderingSystem in the universe
```

### Factory

Method that builds an `Entity` from some parameters.

```c#
class Factory
{
	private readonly Universe universe;

	private readonly Texture playerTexture;
	private readonly Texture houseTexture;
	private readonly Texture dogTexture;

	public Factories(Universe universe) { … }

	public Entity CreateHouse(float x, float y)
	{
		Entity house = universe.CreateEntity();
		house.AddComponent(new Transform2D(x, y));  // give it a position in 2D space
		house.AddComponent(new Sprite2D(houseTexture));  // give it a sprite
		return house;
	}

	public Entity CreatePlayer(float x, float y)
	{
		Entity player = universe.CreateEntity();
		player.AddComponent(new Transform2D(x, y));
		player.AddComponent(new Sprite2D(playerTexture));
		player.AddComponent(new Creature() { Health = 100 });
		player.AddComponent<FollowPointer>();  // make it controllable by moving the mouse
		player.AddComponent<Friendly>();
		return player;
	}

	public Entity CreateDog(float x, float y)
	{
		Entity dog = universe.CreateEntity();
		dog.AddComponent(new Transform2D(x, y));
		dog.AddComponent(new Sprite2D(dogTexture));
		dog.AddComponent(new Creature() { Health = 50 });
		dog.AddComponent<DogAI>();  // give this entity the brain of a dog
		return dog;
	}
}
```

## Example

```c#
var logger = new ConsoleLog();

// create a universe and register all used components
Universe universe = Universe.Create(logger, true);
universe.RegisterProcessor(new RenderingProcessor());
universe.RegisterComponent<Transform2D>();
universe.RegisterComponent<Sprite2D>();
universe.RegisterComponent<Friendly>();
universe.RegisterComponent<Aggressive>();
universe.RegisterComponent<FollowPointer>();
universe.RegisterComponent<Creature>();
universe.RegisterComponent<DogAI>();
universe.RegisterSystem(new FollowPointerSystem());
universe.RegisterSystem(new DogAISystem());
universe.Build();

// create a factory
var factory = new Factory(universe);

// spawn a house at position [10, 10]
Entity house = factory.CreateHouse(10, 10);

// spawn a player at position [40, 100]
Entity player = factory.CreatePlayer(40, 100);

// spawn a friendly dog that will fight aggressive entities
Entity friendlyDog = factory.CreateDog(40, 100);
friendlyDog.AddComponent<Friendly>();  // flag as friendly

// spawn an aggressive dog that will fight friendly entities
Entity aggressiveDog = factory.CreateDog(300, 200);
aggressiveDog.AddComponent<Aggressive>();  // flag as aggressive

while (true)
{
	universe.Execute(…);

	// TODO: render the result.
}
```

## License

This software is released under the terms of [The MIT License](LICENSE).
