using GameEngine.Native.Internal;
using System;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;

namespace GameEngine.Native.Internal
{
	static class CharConverter
	{
		static readonly string[] strs = Enumerable.Range(0x00, 0x80).Select(i => Char.ConvertFromUtf32(i)).ToArray();

		public static string FromUTF32(int utf32)
		{
			if (utf32 < strs.Length)
				return strs[utf32];

			return Char.ConvertFromUtf32(utf32);
		}

		public static string FromUTF16(char utf16)
		{
			if (utf16 < strs.Length)
				return strs[utf16];

			return new String(utf16, 1);
		}
	}
}
