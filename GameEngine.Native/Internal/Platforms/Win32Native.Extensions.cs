using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Runtime.InteropServices;

namespace GameEngine.Native.Internal.Platforms
{
	static partial class Win32
	{
		static readonly int GetRawInputData_bRawDataOffset = (int)Marshal.OffsetOf<RAWINPUT>(nameof(RAWINPUT.data)) + (int)Marshal.OffsetOf<RAWHID>(nameof(RAWHID.bRawData));
		static readonly int GetRawInputData_cbSizeHeader = Marshal.SizeOf(typeof(RAWINPUTHEADER));
		static readonly int RegisterRawInputDevices_cbSize = Marshal.SizeOf(typeof(RAWINPUTDEVICE));

		public static bool RegisterRawInputDevices(RAWINPUTDEVICE[] pRawInputDevices) => RegisterRawInputDevices(pRawInputDevices, pRawInputDevices.Length, RegisterRawInputDevices_cbSize);

		public static unsafe int GetRawInputDataBufferSize(IntPtr hRawInput)
		{
			var pcbSize = 0;
			var result = GetRawInputData(hRawInput, RID.INPUT, IntPtr.Zero, ref pcbSize, GetRawInputData_cbSizeHeader);
			if (result != 0)
				throw new Exception();

			return Math.Max(0, pcbSize - GetRawInputData_bRawDataOffset);
		}

		public static unsafe RAWINPUT_Managed GetRawInputData(IntPtr hRawInput, Span<byte> buffer)
		{
			var pcbSize = 0;
			var result = GetRawInputData(hRawInput, RID.INPUT, IntPtr.Zero, ref pcbSize, GetRawInputData_cbSizeHeader);
			if (result != 0)
				throw new Exception();

			if (buffer.Length < pcbSize - GetRawInputData_bRawDataOffset)
				throw new ArgumentException("Insufficient buffer length", nameof(buffer));

			byte* data = stackalloc byte[pcbSize];

			fixed (byte* pBuffer = &buffer[0])
			{
				result = GetRawInputData(hRawInput, RID.INPUT, new IntPtr(data), ref pcbSize, GetRawInputData_cbSizeHeader);
				Debug.Assert(result == pcbSize);
				if (result == -1)
					throw new Exception();

				var pDataNative = Marshal.PtrToStructure<RAWINPUT>(new IntPtr(data));

				var pDataConverted = new RAWINPUT_Managed
				{
					header = pDataNative.header,
					data = new RAWINPUT_Managed.DATA
					{
						mouse = new RAWMOUSE_Managed
						{
							usFlags = pDataNative.data.mouse.usFlags,
							ulButtons = pDataNative.data.mouse.ulButtons,
							ulRawButtons = pDataNative.data.mouse.ulRawButtons,
							lLastX = pDataNative.data.mouse.lLastX,
							lLastY = pDataNative.data.mouse.lLastY,
							ulExtraInformation = pDataNative.data.mouse.ulExtraInformation,
						}
					}
				};

				if (pDataNative.header.dwType == RIM.TYPEHID)
					pDataConverted.data_hid_bRawData = buffer.Slice(0, pcbSize - GetRawInputData_bRawDataOffset);

				return pDataConverted;
			}
		}

		[StructLayout(LayoutKind.Sequential)]
		public ref struct RAWINPUT_Managed
		{
			[StructLayout(LayoutKind.Explicit)]
			public ref struct DATA
			{
				[FieldOffset(0)]
				public RAWMOUSE_Managed mouse;
				[FieldOffset(0)]
				public RAWKEYBOARD_Managed keyboard;
				[FieldOffset(0)]
				public RAWHID_Managed hid;
			}

			public RAWINPUTHEADER header;
			public DATA data;
			public Span<byte> data_hid_bRawData;
		}

		[StructLayout(LayoutKind.Sequential)]
		public ref struct RAWMOUSE_Managed
		{
			public MOUSE usFlags;
			public uint ulButtons;

			public RI_MOUSE usButtonFlags { get => (RI_MOUSE)unchecked((ushort)(ulButtons)); set => ulButtons = unchecked(ulButtons & (uint)0xffff0000) | (uint)value; }
			public ushort usButtonData { get => unchecked((ushort)(ulButtons >> 16)); set => ulButtons = unchecked(ulButtons & (uint)0x0000ffff) | (uint)value << 16; }

			public uint ulRawButtons;
			public int lLastX;
			public int lLastY;
			public uint ulExtraInformation;
		}

		[StructLayout(LayoutKind.Sequential)]
		public ref struct RAWKEYBOARD_Managed
		{
			public ushort MakeCode;
			public RI_KEY Flags;
			public ushort Reserved;
			public VK VKey;
			public WM Message;
			public uint ExtraInformation;
		}

		[StructLayout(LayoutKind.Sequential)]
		public ref struct RAWHID_Managed
		{
			public int dwSizeHid;
			public int dwCount;
		}
	}
}
