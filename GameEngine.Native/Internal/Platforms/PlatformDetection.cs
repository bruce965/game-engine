using System;
using System.Runtime.InteropServices;

namespace GameEngine.Native.Internal.Platforms
{
	enum Platform
	{
		Unknown,
		Win32,
		X11,
	}

	static class PlatformDetection
	{
		static readonly object currentPlatform_lock = new object();
		static Platform? currentPlatform;

		public static Platform CurrentPlatform
		{
			get
			{
				if (!currentPlatform.HasValue)
				{
					lock (currentPlatform_lock)
					{
						var platformToLibrary = new(Platform platform, Type library)[] {
							(Platform.Win32, typeof(Win32)),
							(Platform.X11, typeof(X11.Xlib))
						};

						// iterate all platform and return the first one which's DLL exists
						foreach (var (platform, library) in platformToLibrary)
						{
							try
							{
								Marshal.PrelinkAll(library);
								currentPlatform = platform;
								return currentPlatform.Value;
							}
							catch (DllNotFoundException) { }
							catch (EntryPointNotFoundException) { }
						}

						if (!currentPlatform.HasValue)
							currentPlatform = Platform.Unknown;
					}
				}

				return currentPlatform.Value;
			}
		}
	}
}
