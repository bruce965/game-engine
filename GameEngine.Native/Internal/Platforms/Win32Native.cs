﻿using System;
using System.Runtime.InteropServices;

namespace GameEngine.Native.Internal.Platforms
{
	static partial class Win32
	{
		public static readonly IntPtr TRUE = new IntPtr(1);
		public static readonly IntPtr UNICODE_NOCHAR = new IntPtr(65535);

		public static ushort LOWORD(IntPtr param) => unchecked((ushort)(long)param);
		public static ushort HIWORD(IntPtr param) => unchecked((ushort)((long)param >> 16));

		public static short GET_X_LPARAM(IntPtr param) => unchecked((short)(long)param);
		public static short GET_Y_LPARAM(IntPtr param) => unchecked((short)((long)param >> 16));

		[DllImport("kernel32")]
		public static extern IntPtr GetModuleHandleW(string moduleName);

		[DllImport("user32")]
		public static extern ushort RegisterClassExW(ref WNDCLASSEXW lpwcx);

		[DllImport("user32")]
		public static extern IntPtr LoadIconW(IntPtr hInstance, IntPtr lpIconName);

		[DllImport("user32")]
		public static extern IntPtr LoadCursorW(IntPtr hInstance, IntPtr lpIconName);

		[DllImport("user32", CharSet = CharSet.Unicode)]
		public static extern IntPtr CreateWindowExW(
			uint dwExStyle,
			IntPtr lpClassName,
			string lpWindowName,
			WS dwStyle,
			int x,
			int y,
			int nWidth,
			int nHeight,
			IntPtr hwndParent,
			IntPtr hMenu,
			IntPtr hInstance,
			IntPtr lpParam
		);

		[DllImport("user32")]
		public static extern IntPtr DefWindowProcW(IntPtr hwnd, WM msg, IntPtr wparam, IntPtr lparam);

		[DllImport("user32")]
		public static extern int GetMessage(
			out MSG msg,
			IntPtr hwnd,
			uint wMsgFilterMin,
			uint wMsgFilterMax
		);

		[DllImport("user32")]
		public static extern int PeekMessage(
			out MSG msg,
			IntPtr hwnd,
			uint wMsgFilterMin,
			uint wMsgFilterMax,
			PM wRemoveMsg
		);

		[DllImport("user32")]
		[return: MarshalAs(UnmanagedType.Bool)]
		public static extern bool TranslateMessage(ref MSG lpMsg);

		[DllImport("user32")]
		public static extern IntPtr DispatchMessage(ref MSG lpMsg);

		[DllImport("user32")]
		public static extern void PostQuitMessage(int exitCode);

		[DllImport("user32")]
		[return: MarshalAs(UnmanagedType.Bool)]
		public static extern bool DestroyWindow(IntPtr hwnd);

		[DllImport("user32")]
		[return: MarshalAs(UnmanagedType.Bool)]
		public static extern bool PostMessage(IntPtr hwnd, WM msg, IntPtr wparam, IntPtr lparam);

		[DllImport("user32")]
		[return: MarshalAs(UnmanagedType.Bool)]
		public static extern bool AdjustWindowRectEx(ref RECT lpRect, WS dwStyle, int bMenu, uint dwExStyle);

		[DllImport("user32")]
		[return: MarshalAs(UnmanagedType.Bool)]
		public static extern bool ShowWindow(IntPtr hwnd, SW nCmdShow);

		[DllImport("user32")]
		[return: MarshalAs(UnmanagedType.Bool)]
		public static extern bool UpdateWindow(IntPtr hwnd);

		[DllImport("user32")]
		[return: MarshalAs(UnmanagedType.Bool)]
		public static extern bool ValidateRect(IntPtr hwnd, IntPtr lpRect);

		[DllImport("user32")]
		public static extern int GetRawInputData(IntPtr hRawInput, RID uiCommand, out RAWINPUT pData, ref int pcbSize, int cbSizeHeader);
		[DllImport("user32")]
		public static extern int GetRawInputData(IntPtr hRawInput, RID uiCommand, [Out] IntPtr pData, ref int pcbSize, int cbSizeHeader);

		[DllImport("user32")]
		public static extern bool RegisterRawInputDevices([MarshalAs(UnmanagedType.LPArray, SizeParamIndex = 1)] RAWINPUTDEVICE[] pRawInputDevices, int uiNumDevices, int cbSize);

		[UnmanagedFunctionPointer(CallingConvention.StdCall)]
		public delegate IntPtr WNDPROC(IntPtr hwnd, WM msg, IntPtr wparam, IntPtr lparam);

		[StructLayout(LayoutKind.Sequential)]
		public struct WNDCLASSEXW
		{
			public uint cbSize;
			public CS style;
			public IntPtr lpfnWndProc;
			public int cbClsExtra;
			public int cbWndExtra;
			public IntPtr hInstance;
			public IntPtr hIcon;
			public IntPtr hCursor;
			public IntPtr hbrBackground;
			[MarshalAs(UnmanagedType.LPWStr)]
			public string lpszMenuName;
			[MarshalAs(UnmanagedType.LPWStr)]
			public string lpszClassName;
			public IntPtr hIconSm;
		}

		[StructLayout(LayoutKind.Sequential)]
		public struct MSG
		{
			public IntPtr hwnd;
			public WM message;
			public IntPtr wParam;
			public IntPtr lParam;
			public uint time;
			public POINT pt;
		}

		[StructLayout(LayoutKind.Sequential)]
		public struct POINT
		{
			public int x;
			public int y;
		}

		[StructLayout(LayoutKind.Sequential)]
		public struct RECT
		{
			public int left;
			public int top;
			public int right;
			public int bottom;
		}

		public enum CS : uint
		{
			HREDRAW = 0x2,
			VREDRAW = 0x1,
			OWNDC = 0x20
		}

		public enum WS : uint
		{
			MAXIMIZEBOX = 0x10000,
			MINIMIZEBOX = 0x20000,
			THICKFRAME = 0x40000,
			SYSMENU = 0x80000,
			CAPTION = 0xC00000,
			VISIBLE = 0x10000000,
			OVERLAPPEDWINDOW = SYSMENU | THICKFRAME | CAPTION | MAXIMIZEBOX | MINIMIZEBOX
		}

		public enum WM : uint
		{
			DESTROY = 0x2,
			SIZE = 0x5,
			PAINT = 0xF,
			CLOSE = 0x10,
			QUIT = 0x12,
			ERASEBKGND = 0x14,
			INPUT = 0xFF,
			KEYDOWN = 0x100,
			KEYUP = 0x101,
			CHAR = 0x102,
			UNICHAR = 0x109,
			MOUSEMOVE = 0x200,
			IME_CHAR = 0x286,
			USER = 0x400,
		}

		public enum SW
		{
			SHOWDEFAULT = 10
		}

		public enum PM
		{
			NOREMOVE = 0x0,
			REMOVE = 0x1,
			NOYIELD = 0x2
		}

		public static class CW
		{
			public const int USEDEFAULT = unchecked((int)0x80000000);
		}

		public static class IDI
		{
			public static readonly IntPtr APPLICATION = new IntPtr(32512);
		}

		public static class IDC
		{
			public static readonly IntPtr ARROW = new IntPtr(32512);
		}

		[StructLayout(LayoutKind.Sequential)]
		public struct RAWINPUTHEADER
		{
			public RIM dwType;
			public int dwSize;
			public IntPtr hDevice;
			public IntPtr wParam;
		}

		[StructLayout(LayoutKind.Sequential)]
		public struct RAWINPUT
		{
			[StructLayout(LayoutKind.Explicit)]
			public struct DATA
			{
				[FieldOffset(0)]
				public RAWMOUSE mouse;
				[FieldOffset(0)]
				public RAWKEYBOARD keyboard;
				[FieldOffset(0)]
				public RAWHID hid;
			}

			public RAWINPUTHEADER header;
			public DATA data;
		}

		public enum RIM : int
		{
			TYPEMOUSE = 0,
			TYPEKEYBOARD = 1,
			TYPEHID = 2,
		}

		public enum RID : uint
		{
			HEADER = 0x10000005,
			INPUT = 0x10000003,
		}

		[StructLayout(LayoutKind.Sequential)]
		public struct RAWMOUSE
		{
			public MOUSE usFlags;
			public uint ulButtons;

			public RI_MOUSE usButtonFlags { get => (RI_MOUSE)unchecked((ushort)(ulButtons)); set => ulButtons = unchecked(ulButtons & (uint)0xffff0000) | (uint)value; }
			public ushort usButtonData { get => unchecked((ushort)(ulButtons >> 16)); set => ulButtons = unchecked(ulButtons & (uint)0x0000ffff) | (uint)value << 16; }

			public uint ulRawButtons;
			public int lLastX;
			public int lLastY;
			public uint ulExtraInformation;
		}

		[StructLayout(LayoutKind.Sequential)]
		public struct RAWKEYBOARD
		{
			public ushort MakeCode;
			public RI_KEY Flags;
			public ushort Reserved;
			public VK VKey;
			public uint Message;
			public uint ExtraInformation;
		}

		[StructLayout(LayoutKind.Sequential)]
		public unsafe struct RAWHID
		{
			// https://msdn.microsoft.com/en-us/library/windows/desktop/ms645549(v=vs.85).aspx
			public int dwSizeHid;
			public int dwCount;
			//[MarshalAs(UnmanagedType.ByValArray, SizeConst = 0)]
			public fixed byte bRawData[1];
		}

		public enum MOUSE : ushort
		{
			MOVE_RELATIVE = 0,
			MOVE_ABSOLUTE = 1,
			VIRTUAL_DESKTOP = 2,
			ATTRIBUTES_CHANGED = 4
		}

		public enum RI_MOUSE : ushort
		{
			LEFT_BUTTON_DOWN = 0x1,
			LEFT_BUTTON_UP = 0x2,
			MIDDLE_BUTTON_DOWN = 0x10,
			MIDDLE_BUTTON_UP = 0x20,
			RIGHT_BUTTON_DOWN = 0x4,
			RIGHT_BUTTON_UP = 0x8,
			BUTTON_1_DOWN = 0x1,
			BUTTON_1_UP = 0x2,
			BUTTON_2_DOWN = 0x4,
			BUTTON_2_UP = 0x8,
			BUTTON_3_DOWN = 0x10,
			BUTTON_3_UP = 0x20,
			BUTTON_4_DOWN = 0x40,
			BUTTON_4_UP = 0x80,
			BUTTON_5_DOWN = 0x100,
			BUTTON_5_UP = 0x200,
			WHEEL = 0x400,
		}

		public enum RI_KEY : ushort
		{
			MAKE = 0,
			BREAK = 1,
			E0 = 2,
			E1 = 4,
		}

		public enum VK : ushort
		{
			// https://msdn.microsoft.com/en-us/library/windows/desktop/dd375731(v=vs.85).aspx
			// TODO
		}

		public struct RAWINPUTDEVICE
		{
			// http://www.usb.org/developers/hidpage/Hut1_12v2.pdf

			public ushort usUsagePage;
			public ushort usUsage;
			public RIDEV  dwFlags;
			public IntPtr hwndTarget;
		}

		[Flags]
		public enum RIDEV : uint
		{
			APPKEYS = 0x400,
			CAPTUREMOUSE = 0x200,
			DEVNOTIFY = 0x2000,
			EXCLUDE = 0x10,
			EXINPUTSINK = 0x1000,
			INPUTSINK = 0x100,
			NOHOTKEYS = 0x200,
			NOLEGACY = 0x30,
			PAGEONLY = 0x20,
			REMOVE = 0x1,
		}
	}
}
