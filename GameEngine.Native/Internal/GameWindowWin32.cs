﻿using GameEngine.Native.Internal.Platforms;
using System;
using System.Runtime.InteropServices;

using static GameEngine.Native.Internal.Platforms.Win32;

namespace GameEngine.Native.Internal
{
	internal class GameWindowWin32 : IGameWindow, IDisposable
	{
		public IntPtr Handle { get; private set; }

		readonly WNDPROC wndProc;

		public string Title { get; }
		public int Width { get; private set; }
		public int Height { get; private set; }

		public (int X, int Y) Mouse { get; private set; }

		public event ResizedEventHandler Resized;
		public event ClosingEventHandler Closing;
		public event ErasingBackgroundEventHandler ErasingBackground;
		public event MouseMovedEventHandler MouseMoved;
		public event KeyEventHandler KeyDown;
		public event KeyEventHandler KeyUp;
		public event KeyPressEventHandler KeyPress;

		public GameWindowWin32(string title, int width, int height)
		{
			wndProc = WndProc;  // prevent garbage collection: keep a reference to the closure that will be passed to native code

			Title = title;
			Width = width;
			Height = height;

			var instance = GetModuleHandleW(null);
			var icon = LoadIconW(IntPtr.Zero, IDI.APPLICATION);

			// register the window class
			var wndclass = new WNDCLASSEXW
			{
				cbSize = (uint)Marshal.SizeOf<WNDCLASSEXW>(),
				style = CS.HREDRAW | CS.VREDRAW | CS.OWNDC,
				hInstance = instance,
				hIcon = icon,
				hIconSm = icon,
				hCursor = LoadCursorW(IntPtr.Zero, IDC.ARROW),
				lpszClassName = "GameWindow",
				lpfnWndProc = Marshal.GetFunctionPointerForDelegate(wndProc)
			};

			var atom = RegisterClassExW(ref wndclass);
			if (atom == 0)
				throw new InvalidOperationException("Failed to register window class");

			// size the client area appropriately
			var styles = WS.OVERLAPPEDWINDOW;
			var rect = new RECT { left = 0, top = 0, right = width, bottom = height };
			if (AdjustWindowRectEx(ref rect, styles, 0, 0))
			{
				width = rect.right - rect.left;
				height = rect.bottom - rect.top;
			}

			// create the window
			Handle = CreateWindowExW(
				0,
				new IntPtr(atom),
				title,
				styles,
				CW.USEDEFAULT,
				CW.USEDEFAULT,
				width,
				height,
				IntPtr.Zero,
				IntPtr.Zero,
				instance,
				IntPtr.Zero
			);

			RegisterRawInputDevices(new[]
			{
				new RAWINPUTDEVICE { usUsagePage = 1, usUsage = 2 },  // mouse
				new RAWINPUTDEVICE { usUsagePage = 1, usUsage = 4 },  // joysticks
				new RAWINPUTDEVICE { usUsagePage = 1, usUsage = 5 },  // game pads
				new RAWINPUTDEVICE { usUsagePage = 1, usUsage = 6, dwFlags = RIDEV.NOLEGACY },  // keyboard
			});

			if (Handle == IntPtr.Zero)
				throw new InvalidOperationException("Failed to create window");
		}

		public bool ProcessMessages()
		{
			while (PeekMessage(out var msg, IntPtr.Zero, 0, 0, PM.REMOVE) > 0)
			{
				TranslateMessage(ref msg);
				DispatchMessage(ref msg);

				if (msg.message == WM.QUIT)
					return false;
			}

			return true;
		}

		IntPtr WndProc(IntPtr hwnd, WM msg, IntPtr wparam, IntPtr lparam)
		{
			ScanCode scanCode;

			switch (msg)
			{
				case WM.DESTROY:
					PostQuitMessage(0);
					return IntPtr.Zero;

				case WM.SIZE:
					Width = LOWORD(lparam);
					Height = HIWORD(lparam);
					Resized?.Invoke(this, Width, Height);
					return IntPtr.Zero;

				case WM.PAINT:
					ValidateRect(hwnd, IntPtr.Zero);
					return IntPtr.Zero;

				case WM.CLOSE:
					Closing?.Invoke(this);
					return IntPtr.Zero;

				case WM.ERASEBKGND:
					ErasingBackground?.Invoke(this);
					return TRUE;

				case WM.MOUSEMOVE:
					Mouse = (GET_X_LPARAM(lparam), GET_Y_LPARAM(lparam));
					MouseMoved?.Invoke(this, Mouse.X, Mouse.Y);
					return IntPtr.Zero;

				case WM.KEYDOWN:
					scanCode = Win32ScanCodes.Convert((byte)((long)lparam >> 16));
					if ((((long)lparam >> 30) & 1) == 0)  // if key is being held, KEYDOWN message is repeated with lparam bit 30 set to 1
						KeyDown?.Invoke(this, scanCode);
					KeyPress?.Invoke(this, scanCode, null);
					return IntPtr.Zero;

				case WM.KEYUP:
					scanCode = Win32ScanCodes.Convert((byte)((long)lparam >> 16));
					KeyUp?.Invoke(this, scanCode);
					return IntPtr.Zero;

				// case WM.UNICHAR:
				// 	if (wparam == Macro.UNICODE_NOCHAR)
				// 		return Macro.TRUE;
				// 	KeyPress?.Invoke(this, Char.ConvertFromUtf32((int)wparam));
				// 	return IntPtr.Zero;

				case WM.CHAR:
					KeyPress?.Invoke(this, ScanCode.None, CharConverter.FromUTF16(unchecked((char)wparam)));
					return IntPtr.Zero;

				case WM.IME_CHAR:
					KeyPress?.Invoke(this, ScanCode.None, CharConverter.FromUTF32((int)wparam));
					return IntPtr.Zero;

				// case WM.INPUT:
				// 	Span<byte> buffer = stackalloc byte[GetRawInputDataBufferSize(lparam)];
				// 	var rawInput = GetRawInputData(lparam, buffer);
				// 	if (rawInput.header.dwType == RIM.TYPEKEYBOARD)
				// 	{
				// 		// TODO
				// 		Console.WriteLine((ScanCode)rawInput.data.keyboard.MakeCode);
				// 	}
				// 	return IntPtr.Zero;

				default:
					return DefWindowProcW(hwnd, msg, wparam, lparam);
			}
		}

		public void Show()
		{
			ShowWindow(Handle, SW.SHOWDEFAULT);
			UpdateWindow(Handle);
		}

		public void Close()
		{
			DestroyWindow(Handle);
			Handle = IntPtr.Zero;
		}

		public void Dispose()
		{
			if (Handle != IntPtr.Zero)
				Close();
		}
	}

	static class Win32ScanCodes
	{
		// https://handmade.network/forums/t/2011-keyboard_inputs_-_scancodes,_raw_input,_text_input,_key_names
		// http://download.microsoft.com/download/1/6/1/161ba512-40e2-4cc9-843a-923143f3456c/scancode.doc

		static readonly ScanCode[] scancodes = new[]
		{
			ScanCode.None,
			// TODO
		};

		public static ScanCode Convert(byte nativeScancode)
		{
			if (nativeScancode < scancodes.Length)
				return scancodes[nativeScancode];

			return ScanCode.None;
		}
	}
}
