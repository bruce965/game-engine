using System;

using static X11.X;
using static X11.Xlib;
using static X11.XKBlib;

namespace GameEngine.Native.Internal
{
	internal class GameWindowX11 : IGameWindow, IDisposable
	{
		Display display;
		Window window;

		readonly Atom WM_DELETE_WINDOW;

		public IntPtr Handle => new IntPtr(unchecked((long)(ulong)window));

		string _title;
		public string Title
		{
			get => _title;
			set => XStoreName(display, window, _title = value);
		}

		public int Width { get; private set; }
		public int Height { get; private set; }

		public (int X, int Y) Mouse { get; private set; }

		public event ResizedEventHandler Resized;
		public event ClosingEventHandler Closing;
		public event ErasingBackgroundEventHandler ErasingBackground;
		public event MouseMovedEventHandler MouseMoved;
		public event KeyEventHandler KeyDown;
		public event KeyEventHandler KeyUp;
		public event KeyPressEventHandler KeyPress;

		public GameWindowX11(string title, int width, int height)
		{
			//Title = title;
			Width = width;
			Height = height;

			XInitThreads();

			display = XOpenDisplay(null);
			if (display == Display.None)
				throw new ApplicationException("XOpenDisplay failed");

			var screenNumber = XDefaultScreen(display);
			window = XCreateSimpleWindow(display, XRootWindow(display, screenNumber), 10, 10, (uint)Width, (uint)Height, 1, XBlackPixel(display, screenNumber), XWhitePixel(display, screenNumber));
			XSelectInput(display, window, EventMask.Exposure|EventMask.KeyPress);
			//XGrabPointer();  // TODO
			XMapWindow(display, window);

			Title = title;

			WM_DELETE_WINDOW = XInternAtom(display, "WM_DELETE_WINDOW", false);
			XSetWMProtocols(display, window, WM_DELETE_WINDOW);
		}

		public bool ProcessMessages()
		{
			while (XPending(display) > 0)
			{
				XNextEvent(display, out var evt);
				if (evt.Type == EventType.Expose)
				{
					Width = evt.Expose.Width;
					Height = evt.Expose.Height;
					Resized?.Invoke(this, Width, Height);
				}

				else if (evt.Type == EventType.KeyPress)
				{
					var keysim = XkbKeycodeToKeysym(display, evt.Key.KeyCode, 0, evt.Key.State.HasFlag(KeyMask.Shift) ? 1 : 0);

					// NOTE: next lines are only a test!
					KeyDown?.Invoke(this, (ScanCode)(ulong)keysim);
					KeyPress?.Invoke(this, (ScanCode)(ulong)keysim, "" + evt.Key.KeyCode + "\n");

					/*char buf[128] = {0};
					KeySym keysym;
					int len = XLookupString(&e.xkey, buf, sizeof buf, &keysym, NULL);
					if (keysym == XK_Escape)
						break;*/
				}

				else if (evt.Type == EventType.ClientMessage && (ulong)evt.Client.Data.Int32[0] == (ulong)WM_DELETE_WINDOW)
				{
					Closing?.Invoke(this);
					return false;
				}
			}

			return true;
		}

		public void Show()
		{
			//throw new NotImplementedException();
		}

		public void Close()
		{
			XDestroyWindow(display, window);
			XCloseDisplay(display);

			display = Display.None;
			window = Window.None;
		}

		public void Dispose()
		{
			if (Handle != IntPtr.Zero)
				Close();
		}
	}
}
