using GameEngine.Native.Internal;
using GameEngine.Native.Internal.Platforms;
using System;

namespace GameEngine.Native
{
	#region Events

	public delegate void ResizedEventHandler(IGameWindow sender, int width, int height);

	public delegate void ClosingEventHandler(IGameWindow sender);

	public delegate void ErasingBackgroundEventHandler(IGameWindow sender);

	public delegate void MouseMovedEventHandler(IGameWindow sender, int x, int y);

	public delegate void KeyEventHandler(IGameWindow sender, ScanCode scanCode);

	public delegate void KeyPressEventHandler(IGameWindow sender, ScanCode scanCode, string keyCode);

	#endregion

	public class GameWindow : IGameWindow
	{
		readonly IGameWindow native;

		public IntPtr Handle => native.Handle;

		public string Title => native.Title;

		public int Width => native.Width;

		public int Height => native.Height;

		public (int X, int Y) Mouse => native.Mouse;

		public event ResizedEventHandler Resized { add => native.Resized += value; remove => native.Resized -= value; }
		public event ClosingEventHandler Closing { add => native.Closing += value; remove => native.Closing -= value; }
		public event ErasingBackgroundEventHandler ErasingBackground { add => native.ErasingBackground += value; remove => native.ErasingBackground -= value; }
		public event MouseMovedEventHandler MouseMoved { add => native.MouseMoved += value; remove => native.MouseMoved -= value; }
		public event KeyEventHandler KeyDown { add => native.KeyDown += value; remove => native.KeyDown -= value; }
		public event KeyEventHandler KeyUp { add => native.KeyUp += value; remove => native.KeyUp -= value; }
		public event KeyPressEventHandler KeyPress { add => native.KeyPress += value; remove => native.KeyPress -= value; }

		public GameWindow(string title, int width, int height)
		{
			switch (PlatformDetection.CurrentPlatform)
			{
				case Platform.Win32:
					native = new GameWindowWin32(title, width, height);
					break;

				case Platform.X11:
					native = new GameWindowX11(title, width, height);
					break;

				default:
					throw new PlatformNotSupportedException();
			}
		}

		public bool ProcessMessages() => native.ProcessMessages();

		public void Show() => native.Show();

		public void Close() => native.Close();

		public void Dispose() => native.Dispose();
	}

	public interface IGameWindow : IDisposable
	{
		/// <summary>Native handle.</summary>
		IntPtr Handle { get; }

		string Title { get; /*set;*/ }
		int Width { get; /*set;*/ }
		int Height { get; /*set;*/ }

		(int X, int Y) Mouse { get; }

		event ResizedEventHandler Resized;
		event ClosingEventHandler Closing;
		event ErasingBackgroundEventHandler ErasingBackground;
		event MouseMovedEventHandler MouseMoved;
		event KeyEventHandler KeyDown;
		event KeyEventHandler KeyUp;
		event KeyPressEventHandler KeyPress;

		/// <summary>Process all pending messages.</summary>
		/// <returns><c>false</c> if the loop must be interrupted.</returns>
		bool ProcessMessages();

		void Show();

		void Close();
	}
}
