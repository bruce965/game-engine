﻿using Bgfx;
using GameEngine.Bgfx;
using GameEngine.Common;
using GameEngine.Diagnostics.Logging;
using GameEngine.ECS;
using GameEngine.ECS.Diagnostics;
using GameEngine.Graphics.Graphics2D;
using GameEngine.Native;
using GameEngine.Utility.Text;
using System;
using System.Diagnostics;
using System.Numerics;
using System.Text;

namespace Example
{
	public static class Program
	{
		#if DEBUG
		const bool IsDebugBuild = true;
		#else
		const bool IsDebugBuild = false;
		#endif

		struct TestCoords2D
		{
			public int X, Y;
		}

		struct TestDebugString
		{
			public MutableString Text;
		}

		struct TestFPSString { }

		struct FollowPointer { }

		class DebugTextDrawingSystem : ISystem
		{
			IReadOnlyComponent<TestCoords2D> Coords2D;
			IReadOnlyComponent<TestDebugString> DebugString;

			static readonly byte[] FormatString = Encoding.ASCII.GetBytes("%s");

			public void Initialize(ISystemInitializer system)
			{
				Coords2D = system.ReadsFrom<TestCoords2D>();
				DebugString = system.ReadsFrom<TestDebugString>();
			}

			public unsafe void Execute()
			{
				bgfx.dbg_text_clear(0x00, false);

				var debugStrings = DebugString.ToSpan();
				for (var i = 0; i < debugStrings.Length; i++)
				{
					ref readonly var debugString = ref debugStrings[i];
					ref readonly var coords = ref Coords2D.TryGetFromEntity(debugString.EntityId, out var _).Data;

					fixed (byte* p = debugString.Data.Text.GetBuffer())
					fixed (byte* f = FormatString)
						bgfx.dbg_text_printf((ushort)coords.X, (ushort)coords.Y, 0x1f, f, p);
				}
			}
		}

		class UpdateFPSSystem : ISystem
		{
			IReadOnlyComponent<TestFPSString> FPSString;
			IComponent<TestDebugString> DebugString;
			IDiagnosticsData Diagnostics;

			public void Initialize(ISystemInitializer system)
			{
				FPSString = system.ReadsFrom<TestFPSString>();
				DebugString = system.WritesTo<TestDebugString>();
				Diagnostics = system.ReadsSharedData<IDiagnosticsData>();
			}

			public void Execute()
			{
				var fpsStrings = FPSString.ToSpan();
				for (var i = 0; i < fpsStrings.Length; i++)
				{
					ref readonly var fpsString = ref fpsStrings[i];
					ref var debugString = ref DebugString.TryGetFromEntity(fpsString.EntityId, out var _).Data;

					if (debugString.Text == null)
						debugString.Text = new MutableString(Encoding.ASCII);

					var builder = new MutableStringBuilderASCII(debugString.Text);
					builder.Clear().Append(Math.Round(Diagnostics.FPS), " FPS").Terminate();
				}
			}
		}

		class FollowPointerSystem : ISystem
		{
			IReadOnlyComponent<FollowPointer> FollowPointer;
			IComponent<Transform2D> Transform2D;
			IInputData Input;

			public void Initialize(ISystemInitializer system)
			{
				FollowPointer = system.ReadsFrom<FollowPointer>();
				Transform2D = system.WritesTo<Transform2D>();
				Input = system.ReadsSharedData<IInputData>();
			}

			public void Execute()
			{
				var controlledByKeyboard = FollowPointer.ToSpan();
				for (var i = 0; i < controlledByKeyboard.Length; i++)
				{
					ref readonly var entityId = ref controlledByKeyboard[i].EntityId;

					ref var transform = ref Transform2D.GetFromEntity(entityId).Data;
					transform.Transform.Translation = Input.Pointer;
				}
			}
		}

		[STAThread]
		public static unsafe void Main(string[] args)
		{
			var logger = new ConsoleLogger();
			AppDomain.CurrentDomain.UnhandledException += (sender, e) =>
			{
				if (e.IsTerminating)
				{
					logger.Fatal(e.ExceptionObject);

					// to avoid development delays, terminate only if no debugger is attached
					// since the error is terminating, the session will not continue anyway
					if (!Debugger.IsAttached)
						Environment.Exit(-1);

					return;
				}

				logger.Error(e.ExceptionObject);
			};

			logger.Info("Engine starting...");

			using var window = new GameWindow("Test", 1280, 720);

			var data = new bgfx.PlatformData { nwh = (void*)window.Handle };
			bgfx.set_platform_data(&data);

			var init = new bgfx.Init();
			bgfx.init_ctor(&init);
			var inited = bgfx.init(&init);

			var universe = Universe.Create(logger, IsDebugBuild);
			universe.RegisterProcessor<BgfxRenderingProcessor>();
			//universe.RegisterProcessor(TCPNetworkProcessor.Server(TODO));
			universe.RegisterComponent<Transform2D>();
			universe.RegisterComponent<Sprite2D>();
			universe.RegisterComponent<TestCoords2D>();
			universe.RegisterComponent<TestDebugString>();
			universe.RegisterComponent<TestFPSString>();
			universe.RegisterComponent<FollowPointer>();
			universe.RegisterSystem<UpdateFPSSystem>();
			universe.RegisterSystem<DebugTextDrawingSystem>();
			universe.RegisterSystem<FollowPointerSystem>();

			// TEMP_FIX
			var _viewportData = new ViewportData();
			var _inputData = new InputData();
			universe.RegisterSharedData<IViewportData>(_viewportData);
			universe.RegisterSharedData<ViewportData>(_viewportData);
			universe.RegisterSharedData<InputData>(_inputData);
			universe.RegisterSharedData<IInputData>(_inputData);
			// /TEMP_FIX

			universe.Build();

			var diagnosticsData = universe.GetSharedData<IDiagnosticsData>();
			var viewportData = universe.GetSharedData<ViewportData>();
			var inputData = universe.GetSharedData<InputData>();

			var entityFPS = universe.CreateEntity();
			entityFPS.AddComponent<TestDebugString>();
			entityFPS.AddComponent<TestFPSString>();

			var entitySprite = universe.CreateEntity();
			entitySprite.AddComponent<Transform2D>();
			entitySprite.AddComponent<FollowPointer>();
			entitySprite.AddComponent(new Sprite2D(null) { Size = new Vector2(256, 256) });

			window.ErasingBackground += win => bgfx.touch(0);

			window.Resized += (win, width, height) =>
			{
				viewportData.Width = width;
				viewportData.Height = height;

				bgfx.reset((uint)width, (uint)height, (uint)bgfx.ResetFlags.Vsync, bgfx.TextureFormat.Unknown);
				bgfx.set_view_rect(0, 0, 0, (ushort)width, (ushort)height);
			};

			window.Closing += win => window.Close();

			window.MouseMoved += (win, x, y) => inputData.Pointer = new Vector2(x, y);
			window.KeyDown += (win, code) => Console.WriteLine($"KeyDown: {code}");
			window.KeyUp += (win, code) => Console.WriteLine($"KeyUp: {code}");
			window.KeyPress += (win, code, key) => Console.WriteLine($"KeyPress: {code} {key}");

			window.Show();

			bgfx.set_debug((uint)bgfx.DebugFlags.Text);

			bgfx.set_view_clear(0, (ushort)(bgfx.ClearFlags.Color | bgfx.ClearFlags.Depth), 0x303030ff, 0, (byte)bgfx.StencilFlags.None);

			// force a full GC collection before entering main loop
			GC.Collect();
			GC.WaitForPendingFinalizers();

			logger.Trace("Entering main loop...");

			// 1. Process events
			while (window.ProcessMessages())
			{
				// 2. Run Systems
				bgfx.set_view_rect(0, 0, 0, (ushort)window.Width, (ushort)window.Height);

				var viewMatrix = Matrix4x4.CreateLookAt(new Vector3(0.0f, 0.0f, -35.0f), Vector3.Zero, Vector3.UnitY);
				var projMatrix = Matrix4x4.CreatePerspectiveFieldOfView((float)Math.PI / 3, (float)window.Width / window.Height, 0.1f, 100.0f);
				bgfx.set_view_transform(0, &viewMatrix.M11, &projMatrix.M11);

				bgfx.touch(0);

				// Bgfx.DebugTextClear();
				// Bgfx.DebugTextWrite(0, 0, DebugColor.Green, DebugColor.Transparent, $"{Math.Round(debug.FPS)} FPS");
				universe.Execute();

				PrintDebugInformations(universe, viewportData, diagnosticsData, inputData, bgfx.get_stats());

				// 3. Render
				bgfx.frame(false);
			}

			logger.Info("Engine stopping...");

			bgfx.shutdown();
		}

		static readonly MutableString debugInfoString = new MutableString(Encoding.ASCII);

		static readonly byte[] FormatString = Encoding.ASCII.GetBytes("%s");

		static unsafe void PrintDebugInformations(IUniverse universe, IViewportData viewport, IDiagnosticsData diagnostics, IInputData input, bgfx.Stats* stats)
		{
			var builder = new MutableStringBuilderASCII(debugInfoString);

			builder.Clear().Append("Viewport Size: ", viewport.Width, "x", viewport.Height).Terminate();
			fixed (byte* b = debugInfoString.GetBuffer())
			fixed (byte* f = FormatString)
				bgfx.dbg_text_printf(2, 1, 0x3f, f, b);

			builder.Clear();
			builder.Append("Managed Memory: ");
			builder.Append(diagnostics.ManagedMemory / 1024);
			builder.Append("KB");
			builder.Terminate();
			fixed (byte* b = debugInfoString.GetBuffer())
			fixed (byte* f = FormatString)
				bgfx.dbg_text_printf(2, 2, 0x3f, f, b);

			builder.Clear();
			builder.Append("GPU Memory: ");
			builder.Append(stats->gpuMemoryUsed / 1024);
			builder.Append("KB");
			builder.Terminate();
			fixed (byte* b = debugInfoString.GetBuffer())
			fixed (byte* f = FormatString)
				bgfx.dbg_text_printf(2, 3, 0x3f, f, b);

			builder.Clear();
			builder.Append("Time: ");
			builder.Append(Math.Round(diagnostics.LastExecutionTime.TotalMilliseconds, 3));
			builder.Append("ms (");
			builder.Append(Math.Round(diagnostics.FPS));
			builder.Append(" FPS)");
			builder.Terminate();
			fixed (byte* b = debugInfoString.GetBuffer())
			fixed (byte* f = FormatString)
				bgfx.dbg_text_printf(2, 4, 0x3f, f, b);

			// --

			builder.Clear();
			builder.Append("Universe: ");
			builder.Append("?");  // builder.Append(universe.Entities.Count());
			builder.Append(" Entities (");
			builder.Append("?");  // builder.Append(universe.Entities.Sum(entity => entity.GetComponents().Count()));
			builder.Append(" Components)");
			builder.Terminate();
			fixed (byte* b = debugInfoString.GetBuffer())
			fixed (byte* f = FormatString)
				bgfx.dbg_text_printf(2, 6, 0x3f, f, b);

			// --

			builder.Clear();
			builder.Append("ECS: ");
			builder.Append(universe.Systems.Count);
			builder.Append(" Systems, ");
			builder.Append(universe.Components.Count);
			builder.Append(" Components");
			builder.Terminate();
			fixed (byte* b = debugInfoString.GetBuffer())
			fixed (byte* f = FormatString)
				bgfx.dbg_text_printf(2, 8, 0x3f, f, b);

			builder.Clear();
			builder.Append("Draw Calls: ");
			builder.Append(stats->numDraw);
			builder.Terminate();
			fixed (byte* b = debugInfoString.GetBuffer())
			fixed (byte* f = FormatString)
				bgfx.dbg_text_printf(2, 9, 0x3f, f, b);

			builder.Clear();
			builder.Append("Draw Calls: ");
			builder.Append(stats->numDraw);
			builder.Terminate();
			fixed (byte* b = debugInfoString.GetBuffer())
			fixed (byte* f = FormatString)
				bgfx.dbg_text_printf(2, 9, 0x3f, f, b);

			builder.Clear();
			builder.Append("Compute Calls: ");
			builder.Append(stats->numCompute);
			builder.Terminate();
			fixed (byte* b = debugInfoString.GetBuffer())
			fixed (byte* f = FormatString)
				bgfx.dbg_text_printf(2, 10, 0x3f, f, b);

			builder.Clear();
			builder.Append("Dynamic Index Buffers: ");
			builder.Append(stats->numDynamicIndexBuffers);
			builder.Terminate();
			fixed (byte* b = debugInfoString.GetBuffer())
			fixed (byte* f = FormatString)
				bgfx.dbg_text_printf(2, 11, 0x3f, f, b);

			builder.Clear();
			builder.Append("Dynamic Vertex Buffers: ");
			builder.Append(stats->numDynamicVertexBuffers);
			builder.Terminate();
			fixed (byte* b = debugInfoString.GetBuffer())
			fixed (byte* f = FormatString)
				bgfx.dbg_text_printf(2, 12, 0x3f, f, b);

			builder.Clear();
			builder.Append("Frame Buffers: ");
			builder.Append(stats->numFrameBuffers);
			builder.Terminate();
			fixed (byte* b = debugInfoString.GetBuffer())
			fixed (byte* f = FormatString)
				bgfx.dbg_text_printf(2, 13, 0x3f, f, b);

			builder.Clear();
			builder.Append("Index Buffers: ");
			builder.Append(stats->numIndexBuffers);
			builder.Terminate();
			fixed (byte* b = debugInfoString.GetBuffer())
			fixed (byte* f = FormatString)
				bgfx.dbg_text_printf(2, 14, 0x3f, f, b);

			builder.Clear();
			builder.Append("Occlusion Queries: ");
			builder.Append(stats->numOcclusionQueries);
			builder.Terminate();
			fixed (byte* b = debugInfoString.GetBuffer())
			fixed (byte* f = FormatString)
				bgfx.dbg_text_printf(2, 15, 0x3f, f, b);

			builder.Clear();
			builder.Append("Programs: ");
			builder.Append(stats->numPrograms);
			builder.Terminate();
			fixed (byte* b = debugInfoString.GetBuffer())
			fixed (byte* f = FormatString)
				bgfx.dbg_text_printf(2, 16, 0x3f, f, b);

			builder.Clear();
			builder.Append("Shaders: ");
			builder.Append(stats->numShaders);
			builder.Terminate();
			fixed (byte* b = debugInfoString.GetBuffer())
			fixed (byte* f = FormatString)
				bgfx.dbg_text_printf(2, 17, 0x3f, f, b);

			builder.Clear();
			builder.Append("Textures: ");
			builder.Append(stats->numTextures);
			builder.Terminate();
			fixed (byte* b = debugInfoString.GetBuffer())
			fixed (byte* f = FormatString)
				bgfx.dbg_text_printf(2, 18, 0x3f, f, b);

			builder.Clear();
			builder.Append("Uniforms: ");
			builder.Append(stats->numUniforms);
			builder.Terminate();
			fixed (byte* b = debugInfoString.GetBuffer())
			fixed (byte* f = FormatString)
				bgfx.dbg_text_printf(2, 19, 0x3f, f, b);

			builder.Clear();
			builder.Append("Vertex Buffers: ");
			builder.Append(stats->numVertexBuffers);
			builder.Terminate();
			fixed (byte* b = debugInfoString.GetBuffer())
			fixed (byte* f = FormatString)
				bgfx.dbg_text_printf(2, 20, 0x3f, f, b);

			builder.Clear();
			builder.Append("Vertex Declarations: ");
			builder.Append(stats->numVertexLayouts);
			builder.Terminate();
			fixed (byte* b = debugInfoString.GetBuffer())
			fixed (byte* f = FormatString)
				bgfx.dbg_text_printf(2, 21, 0x3f, f, b);

			// --

			builder.Clear();
			builder.Append("Pointer: [");
			builder.Append(input.Pointer.X);
			builder.Append(", ");
			builder.Append(input.Pointer.Y);
			builder.Append("]");
			builder.Terminate();
			fixed (byte* b = debugInfoString.GetBuffer())
			fixed (byte* f = FormatString)
				bgfx.dbg_text_printf(2, 23, 0x3f, f, b);
		}
	}
}
