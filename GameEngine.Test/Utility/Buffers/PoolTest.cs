using System;
using GameEngine.Utility.Buffers;
using Xunit;

namespace GameEngine.Test.Utility.Buffer
{
	public class PoolTest
	{
		class MyClass
		{
			public string Test;

			public MyClass() { }

			public MyClass(string test)
			{
				Test = test;
			}
		}

		[Fact]
		public void SharedPoolsAreShared()
		{
			var mySharedPool1 = Pool<MyClass>.Shared;
			var mySharedPool2 = Pool<MyClass>.Shared;

			Assert.Equal(mySharedPool1, mySharedPool2);
			Assert.NotNull(mySharedPool1);
		}

		[Fact]
		public void CustomPoolsAreNotShared()
		{
			var myPool1 = Pool.Create<MyClass>();
			var myPool2 = Pool.Create<MyClass>();

			Assert.NotEqual(myPool1, myPool2);
			Assert.NotNull(myPool1);
		}

		[Fact]
		public void OverflowIsHandled()
		{
			var myPool = Pool.Create<MyClass>(3);

			var myPooledObject1 = myPool.Rent();
			var myPooledObject2 = myPool.Rent();
			var myPooledObject3 = myPool.Rent();
			var myPooledObject4 = myPool.Rent();

			Assert.NotEqual(myPooledObject1, myPooledObject2);

			myPool.Return(myPooledObject1);
			myPool.Return(myPooledObject2);
			myPool.Return(myPooledObject3);
			myPool.Return(myPooledObject4);
		}

		[Fact]
		public void FactoriesAreUsed()
		{
			var myPool = Pool.Create<MyClass>(() => new MyClass("banana"));

			var myPooledObject = myPool.Rent();

			Assert.Equal("banana", myPooledObject.Test);

			myPool.Return(myPooledObject);
		}

		[Fact]
		public void ObjectsAreRecycled()
		{
			var myPool = Pool.Create<MyClass>();

			var myPooledObject = myPool.Rent();
			myPool.Return(myPooledObject);

			var myPooledObject2 = myPool.Rent();

			Assert.Equal(myPooledObject, myPooledObject2);
		}
	}
}
